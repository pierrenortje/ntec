﻿namespace NTEC.Diagnostics
{
    using System.Diagnostics;
    using System.Threading.Tasks;

    public class ProcessControl : EventOccuredBase
    {
        #region Private Fields
        private Process process = null;
        private string fileName = null;
        private bool redirectStandardOutput = true;
        private bool redirectStandardError = true;
        #endregion

        #region Constructors
        public ProcessControl() { }
        public ProcessControl(string fileName, string[] warningMessages, string[] errorMessages)
            : this()
        {
            this.fileName = fileName;
            this.warningMessages = warningMessages;
            this.errorMessages = errorMessages;
        }
        #endregion

        #region Protected Properties
        protected bool RedirectStandardOutput
        {
            set
            {
                this.redirectStandardOutput = value;
            }
        }
        protected bool RedirectStandardError
        {
            set
            {
                this.redirectStandardError = value;
            }
        }
        #endregion

        #region Protected Methods
        protected async Task<bool> ExecuteAsync(params string[] args)
        {
            if (process != null)
            {
                process.WaitForExit();
                process.Close();
                process.Dispose();
                process = null;
            }

            process = new Process();

            process.StartInfo.FileName = fileName;
            process.StartInfo.Arguments = string.Join(" ", args);

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardError = redirectStandardError;
            process.StartInfo.RedirectStandardOutput = redirectStandardOutput;

            if (redirectStandardError)
                process.ErrorDataReceived += Process_ErrorDataReceived;

            if (redirectStandardOutput)
                process.OutputDataReceived += Process_OutputDataReceived;

            process.Start();

            if (redirectStandardError)
                process.BeginErrorReadLine();

            if (redirectStandardOutput)
                process.BeginOutputReadLine();

            await Task.Run(() =>
            {
                process.WaitForExit();
            });

            int exitCode = process.ExitCode;

            process.Close();
            process.Dispose();
            process = null;

            return exitCode == 0;
        }
        #endregion

        #region Private Events
        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null)
                return;

            OnEventOccured(e.Data);
        }
        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null)
                return;

            OnEventOccured(e.Data, EventType.Error);
        }
        #endregion

        #region IDisposable Members
        public override void Dispose()
        {
            if (process != null)
            {
                process.Dispose();
                process = null;
            }

            base.Dispose();
        }
        #endregion
    }
}