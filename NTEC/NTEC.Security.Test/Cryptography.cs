﻿namespace NTEC.Security.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NTEC.Test;
    using System.Security.Cryptography;

    [TestClass]
    public sealed class Cryptography : TestBase
    {
        #region Private Static Fields
        private const string WRONG_PASSWORD = "wrongpassword";
        #endregion

        #region Public Methods
        [TestMethod]
        public void Encrypt_Valid()
        {
            string hashedPassord = NTEC.Security.Cryptography.HashPassword(PASSWORD);
            bool actualValue = NTEC.Security.Cryptography.ValidatePassword(PASSWORD, hashedPassord);

            Assert.IsTrue(actualValue, "Failed to validate password.");
        }

        [TestMethod]
        public void Encrypt_Generic_Valid()
        {
            string expectedValue = "Hello World";
            var cryptoEncrypt = new NTEC.Security.Cryptography();
            string encryptedValue = cryptoEncrypt.Encrypt<RijndaelManaged>(expectedValue, PASSWORD);

            var cryptoDecrypt = new NTEC.Security.Cryptography();
            string actualValue = cryptoDecrypt.Decrypt<RijndaelManaged>(encryptedValue, PASSWORD, cryptoEncrypt.IV);

            Assert.AreEqual(expectedValue, actualValue, "Failed to validate password.");
        }

        [TestMethod]
        public void Encrypt_File_Generic_Valid()
        {
            //string originalFileContents = System.IO.File.ReadAllText(XML_FILE_PATH);

            var cryptoEncrypt = new NTEC.Security.Cryptography();
            cryptoEncrypt.EncryptFile<RijndaelManaged>(XML_FILE_PATH, PASSWORD);

            var cryptoDecrypt = new NTEC.Security.Cryptography();
            cryptoDecrypt.DecryptFile<RijndaelManaged>(XML_FILE_PATH, PASSWORD, cryptoEncrypt.IV);

            //string modifiedFileContents = System.IO.File.ReadAllText(XML_FILE_PATH);

            //bool actualValue = originalFileContents.Equals(modifiedFileContents);

            //Assert.IsTrue(actualValue, "Failed to encrypt file.");
        }

        [TestMethod]
        public void Encrypt_NotValid()
        {
            string hashedPassord = NTEC.Security.Cryptography.HashPassword(PASSWORD);
            bool actualValue = NTEC.Security.Cryptography.ValidatePassword(WRONG_PASSWORD, hashedPassord);

            Assert.IsFalse(actualValue, "Specified password should not be valid.");
        }
        #endregion
    }
}