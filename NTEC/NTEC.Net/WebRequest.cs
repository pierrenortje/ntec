﻿namespace NTEC.Net
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;

    /// <summary>
    /// Makes a request to a Uniform Resource Identifier (URI).
    /// </summary>
    public class WebRequest
    {
        #region Private Fields
        private Uri uri = null;
        private X509Certificate2 certificate = null;
        private System.Net.WebRequest webRequest = null;
        private List<KeyValuePair<string, string>> headers = null;

        private string contentType = null;
        private byte[] contentData = null;
        #endregion

        #region Constructors
        public WebRequest(string url)
        {
            this.uri = GetUri(url);

            InitializeWebRequest();
        }
        public WebRequest(Uri uri) : this(uri.AbsoluteUri) { }
        #endregion

        #region Public Properties
        public string ContentType
        {
            get
            {
                return this.contentType;
            }
            set
            {
                this.contentType = value;
            }
        }
        public List<KeyValuePair<string, string>> Headers
        {
            get
            {
                return this.headers;
            }
            set
            {
                this.headers = value;
            }
        }
        #endregion

        #region Private Methods
        private Uri GetUri(string url)
        {
            Uri uriResult = null;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps))
                throw new InvalidCastException("Invalid URL specified.");

            return uriResult;
        }

        private void InitializeWebRequest()
        {
            this.webRequest = System.Net.WebRequest.Create(this.uri) as HttpWebRequest;
            if (this.webRequest == null)
                throw new UriFormatException("Could not create web request.");
        }

        private void AppendHeaders()
        {
            if (this.headers != null && this.headers.Count > 0)
                foreach (var header in this.headers)
                    this.webRequest.Headers.Set(header.Key, header.Value);
        }
        private void AppendBody(string postData)
        {
            if (!string.IsNullOrEmpty(postData))
            {
                this.contentData = Encoding.ASCII.GetBytes(postData);
                this.webRequest.ContentLength = this.contentData.Length;
            }
        }
        private void AppendParameters(List<KeyValuePair<string, string>> parameters)
        {
            if (parameters != null && parameters.Count > 0)
            {
                var uriBuilder = new UriBuilder(this.uri);
                var queryString = HttpUtility.ParseQueryString(uriBuilder.Query);

                foreach (var parameter in parameters)
                    queryString[parameter.Key] = parameter.Value;

                uriBuilder.Query = queryString.ToString();
                this.uri = uriBuilder.Uri;
                InitializeWebRequest();
            }
        }

        private string SendGetRequest()
        {
            AppendHeaders();

            using (var webResponse = this.webRequest.GetResponse() as HttpWebResponse)
            {
                if (webResponse == null)
                    return null;

                using (var streamReader = new StreamReader(webResponse.GetResponseStream()))
                    return streamReader.ReadToEnd();
            }
        }
        private string SendPostRequest()
        {
            AppendHeaders();

            this.webRequest.Method = WebRequestMethods.Http.Post;
            this.webRequest.ContentType = this.contentType ?? Mime.MediaType.JSON;

            using (var responseStream = this.webRequest.GetRequestStream())
            {
                if (this.contentData != null)
                    responseStream.Write(this.contentData, 0, this.contentData.Length);

                return SendGetRequest();
            }
        }

        private async Task<string> SendGetRequestAsync()
        {
            AppendHeaders();

            using (var webResponse = await this.webRequest.GetResponseAsync() as HttpWebResponse)
            {
                if (webResponse == null)
                    return null;

                using (var streamReader = new StreamReader(webResponse.GetResponseStream()))
                    return await streamReader.ReadToEndAsync();
            }
        }
        private async Task<string> SendPostRequestAsync()
        {
            AppendHeaders();

            this.webRequest.Method = WebRequestMethods.Http.Post;
            this.webRequest.ContentType = this.contentType ?? Mime.MediaType.JSON;

            using (var responseStream = await this.webRequest.GetRequestStreamAsync())
            {
                if (this.contentData != null)
                    await responseStream.WriteAsync(this.contentData, 0, this.contentData.Length);

                return await SendGetRequestAsync();
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Attach a certificate to the web request.
        /// </summary>
        /// <param name="certificatePath">An absolute or relative path to the certificate.</param>
        /// <param name="certificatePassword">Password for the certificate. This parameter is optional.</param>
        public void AttachCertificate(string certificatePath, string certificatePassword = null)
        {
            if (string.IsNullOrEmpty(certificatePath))
                throw new ArgumentException("The certificate path is null or empty.");

            if (!File.Exists(certificatePath))
                throw new FileNotFoundException("The certificate could not be found.");

            if (string.IsNullOrEmpty(certificatePassword))
                this.certificate = new X509Certificate2(certificatePath);
            else
                this.certificate = new X509Certificate2(certificatePath, certificatePassword);
        }

        /// <summary>
        /// Creates a GET-request.
        /// </summary>
        /// <returns>The web response.</returns>
        public string Get()
        {
            return Get(null);
        }
        /// <summary>
        /// Creates a GET-request.
        /// </summary>
        /// <param name="parameters">The parameters to append to the request.</param>
        /// <returns>The web response.</returns>
        public string Get(List<KeyValuePair<string, string>> parameters)
        {
            string webResponse = null;

            AppendParameters(parameters);

            webResponse = SendGetRequest();

            return webResponse;
        }
        /// <summary>
        /// Creates a GET-request as an asynchronous operation.
        /// </summary>
        /// <returns>The web response.</returns>
        public async Task<string> GetAsync()
        {
            return await GetAsync(null);
        }
        /// <summary>
        /// Creates a GET-request as an asynchronous operation.
        /// </summary>
        /// <param name="parameters">The parameters to append to the request.</param>
        /// <returns>The web response.</returns>
        public async Task<string> GetAsync(List<KeyValuePair<string, string>> parameters)
        {
            string webResponse = null;

            AppendParameters(parameters);

            webResponse = await SendGetRequestAsync();

            return webResponse;
        }

        /// <summary>
        /// Creates a POST-request.
        /// </summary>
        /// <returns>The web response.</returns>
        public string Post()
        {
            return Post(null);
        }
        /// <summary>
        /// Creates a POST-request.
        /// </summary>
        /// <param name="postData">The body of the POST-request.</param>
        /// <returns>The web response.</returns>
        public string Post(string postData)
        {
            string webResponse = null;

            AppendBody(postData);

            webResponse = SendPostRequest();

            return webResponse;
        }
        /// <summary>
        /// Creates a POST-request as an asynchronous operation.
        /// </summary>
        /// <returns>The web response.</returns>
        public async Task<string> PostAsync()
        {
            return await PostAsync(null);
        }
        /// <summary>
        /// Creates a POST-request as an asynchronous operation.
        /// </summary>
        /// <param name="postData">The body of the POST-request.</param>
        /// <returns>The web response.</returns>
        public async Task<string> PostAsync(string postData)
        {
            string webResponse = null;

            AppendBody(postData);

            webResponse = await SendPostRequestAsync();

            return webResponse;
        }
        #endregion
    }
}