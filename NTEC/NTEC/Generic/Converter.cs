﻿namespace NTEC.Generic
{
    using System;
    using System.Collections;
    using System.Reflection;

    /// <summary>
    /// Provides methods to convert from one Entity to another and also Lists and vice versa.
    /// </summary>
    public static class Converter
    {
        /// <summary>
        /// Populates an entity's properties with values from matching properties in the given object.
        /// </summary>
        /// <typeparam name="T">The type of entity that will be populated.</typeparam>
        /// <param name="entity">The destination entity's properties that will be set.</param>
        /// <param name="obj">The source object used to get the property values from.</param>
        public static void PopulateEntity<T>(T entity, object obj)
        {
            if (obj == null)
                return;

            Type entity_type = entity.GetType();
            Type obj_type = obj.GetType();

            foreach (PropertyInfo entity_property in entity_type.GetProperties())
            {
                if (!entity_property.CanWrite
                    || entity_property.GetIndexParameters().Length > 0)
                    continue;

                foreach (PropertyInfo obj_property in obj_type.GetProperties())
                {
                    if (!obj_property.CanRead
                        || obj_property.GetIndexParameters().Length > 0)
                        continue;

                    if (!entity_property.Name.Equals(obj_property.Name))
                        continue;

                    object obj_value = obj_property.GetValue(obj, null);
                    if (obj_value == null)
                        continue;

                    if (obj_value == DBNull.Value)
                        entity_property.SetValue(entity, DBNull.Value, null);
                    else
                    {
                        object entity_value = null;
                        if (entity_property.PropertyType.IsEnum)
                        {
                            if (obj_value is int)
                                entity_value = GenericTools.ChangeType(obj_value, entity_property.PropertyType);
                            else
                                entity_value = Enum.Parse(entity_property.PropertyType, Convert.ToString(obj_value));
                        }
                        else
                        {
                            if (obj_property.PropertyType == entity_property.PropertyType)
                                entity_value = obj_value;
                            else if (obj_value is IConvertible)
                                entity_value = GenericTools.ChangeType(obj_value, entity_property.PropertyType);
                            else
                                entity_value = obj_value;
                        }

                        if (entity_value != null)
                            entity_property.SetValue(entity, entity_value, null);
                    }
                }
            }
        }
        /// <summary>
        /// Populates an entity's properties with values from matching properties in the given object.
        /// </summary>
        /// <param name="entity">The destination entity's properties that will be set.</param>
        /// <param name="obj">The source object used to get the property values from.</param>
        public static void PopulateEntity(object entity, object obj)
        {
            if (obj == null)
                return;

            Type entity_type = entity.GetType();
            Type obj_type = obj.GetType();

            foreach (PropertyInfo entity_property in entity_type.GetProperties())
            {
                if (!entity_property.CanWrite
                    || entity_property.GetIndexParameters().Length > 0)
                    continue;

                foreach (PropertyInfo obj_property in obj_type.GetProperties())
                {
                    if (!obj_property.CanRead
                        || obj_property.GetIndexParameters().Length > 0)
                        continue;

                    if (!entity_property.Name.Equals(obj_property.Name))
                        continue;

                    object obj_value = obj_property.GetValue(obj, null);
                    if (obj_value == null)
                        continue;

                    if (obj_value == DBNull.Value)
                        entity_property.SetValue(entity, DBNull.Value, null);
                    else
                    {
                        object entity_value = null;
                        if (entity_property.PropertyType.IsEnum)
                        {
                            if (obj_value is int)
                                entity_value = GenericTools.ChangeType(obj_value, entity_property.PropertyType);
                            else
                                entity_value = Enum.Parse(entity_property.PropertyType, Convert.ToString(obj_value));
                        }
                        else
                        {
                            if (obj_property.PropertyType == entity_property.PropertyType)
                                entity_value = obj_value;
                            else if (obj_value is IConvertible)
                                entity_value = GenericTools.ChangeType(obj_value, entity_property.PropertyType);
                            else
                                entity_value = obj_value;
                        }

                        if (entity_value != null)
                            entity_property.SetValue(entity, entity_value, null);
                    }
                }
            }
        }

        /// <summary>
        /// Populates a list with data from the objects array.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="list">The destination list to populate.</param>
        /// <param name="objects">The source object array used to populate the list with.</param>
        public static void PopulateList<T>(T list, object[] objects)
            where T : IList
        {
            if (objects == null)
                return;

            Type entityType = GenericTools.GetEntityType<T>();
            if (entityType == null)
                return;

            if (list == null)
                list = Activator.CreateInstance<T>();

            for (int i = 0; i < objects.Length; i++)
                list.Add(ToEntity(objects[i], entityType));
        }
        /// <summary>
        /// Populates a list with data from the collection.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="list">The destination list to populate.</param>
        /// <param name="collection">The source collection used to populate the list with.</param>
        public static void PopulateList<T>(T list, IList collection)
            where T : IList
        {
            if (collection == null)
                return;

            Type entityType = GenericTools.GetEntityType<T>();
            if (entityType == null)
                return;

            if (list == null)
                list = Activator.CreateInstance<T>();

            for (int i = 0; i < collection.Count; i++)
                list.Add(ToEntity(collection[i], entityType));
        }
        /// <summary>
        /// Populates a list with data from the enumerable.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="list">The destination list to populate.</param>
        /// <param name="enumerable">The source enumerable used to populate the list with.</param>
        public static void PopulateList<T>(T list, IEnumerable enumerable)
            where T : IList
        {
            if (enumerable == null)
                return;

            Type entityType = GenericTools.GetEntityType<T>();
            if (entityType == null)
                return;

            if (list == null)
                list = Activator.CreateInstance<T>();

            foreach (object c in enumerable)
                list.Add(ToEntity(c, entityType));
        }

        /// <summary>
        /// Populates an array with data from the objects array.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="array">The destination array to populate.</param>
        /// <param name="objects">The source objects used to populate the array with.</param>
        public static void PopulateArray<T>(T[] array, object[] objects)
        {
            if (array == null)
                return;

            if (array == null)
                array = (T[])Array.CreateInstance(typeof(T), objects.Length);

            for (int i = 0; i < objects.Length; i++)
                array[i] = ToEntity<T>(objects[i]);
        }
        /// <summary>
        /// Populates an array with data from the collection.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="array">The destination array to populate.</param>
        /// <param name="collection">The source objects used to populate the array with.</param>
        public static void PopulateArray<T>(T[] array, IList collection)
        {
            if (collection == null)
                return;

            if (array == null)
                array = (T[])Array.CreateInstance(typeof(T), collection.Count);

            for (int i = 0; i < collection.Count; i++)
                array[i] = ToEntity<T>(collection[i]);
        }

        /// <summary>
        /// Converts the object to an entity by matching propery names.
        /// </summary>
        /// <typeparam name="T">The type of entity to convert to.</typeparam>
        /// <param name="obj">The source object used to get the property values from.</param>
        /// <returns>The converted entity.</returns>
        public static T ToEntity<T>(object obj)
        {
            T entity = default(T);
            if (obj == null)
                return entity;

            entity = Activator.CreateInstance<T>();
            PopulateEntity<T>(entity, obj);

            return entity;
        }
        /// <summary>
        /// Converts the object to an entity by matching propery names.
        /// </summary>
        /// <param name="obj">The source object used to get the property values from.</param>
        /// <param name="entityType">The type of entity to create and populate from the object.</param>
        /// <returns>The converted entity.</returns>
        public static object ToEntity(object obj, Type entityType)
        {
            object entity = GenericTools.GetDefaultValue(entityType);
            if (obj == null)
                return entity;

            if (entityType.IsInterface)
                entityType = InterfaceImplementer.GetImplementation(entityType);

            entity = Activator.CreateInstance(entityType);
            PopulateEntity(entity, obj);

            return entity;
        }

        /// <summary>
        /// Converts an object array to a list.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="objects">The source array containing the data used to populate the list with.</param>
        /// <returns></returns>
        public static T ToList<T>(object[] objects)
            where T : IList
        {
            if (objects == null)
                return default(T);

            Type entityType = GenericTools.GetEntityType<T>();
            if (entityType == null)
                return default(T);

            T list = Activator.CreateInstance<T>();
            PopulateList<T>(list, objects);

            return list;
        }
        /// <summary>
        /// Converts an enumerable to a list.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="enumerable">The source enumerable containing the data used to populate the list with.</param>
        /// <returns></returns>
        public static T ToList<T>(IEnumerable enumerable)
            where T : IList
        {
            if (enumerable == null)
                return default(T);

            Type entityType = GenericTools.GetEntityType<T>();
            if (entityType == null)
                return default(T);

            T list = Activator.CreateInstance<T>();
            PopulateList<T>(list, enumerable);

            return list;
        }
        /// <summary>
        /// Converts a collection to a list.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="collection">The source collection containing the data used to populate the list with.</param>
        /// <returns></returns>
        public static T ToList<T>(IList collection)
            where T : IList
        {
            if (collection == null)
                return default(T);

            Type entityType = GenericTools.GetEntityType<T>();
            if (entityType == null)
                return default(T);

            T list = Activator.CreateInstance<T>();
            PopulateList<T>(list, collection);

            return list;
        }

        /// <summary>
        /// Converts an object array to an array.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="objects">The source array containing the data used to populate the list with.</param>
        /// <returns></returns>
        public static T[] ToArray<T>(object[] objects)
        {
            if (objects == null)
                return null;

            T[] array = (T[])Array.CreateInstance(typeof(T), objects.Length);
            PopulateArray<T>(array, objects);

            return array;
        }
        /// <summary>
        /// Converts a collection to an array.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="collection">The source collection containing the data used to populate the list with.</param>
        /// <returns></returns>
        public static T[] ToArray<T>(IList collection)
        {
            if (collection == null)
                return null;

            T[] array = (T[])Array.CreateInstance(typeof(T), collection.Count);
            PopulateArray<T>(array, collection);

            return array;
        }
    }
}