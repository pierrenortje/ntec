﻿namespace NTEC.Xml
{
    using System.IO;
    using System.Xml.Serialization;

    public static class Serialization
    {
        /// <summary>
        /// Writes the given object instance to an XML file.
        /// </summary>
        /// <typeparam name="T">The type of object being written to the file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the file.</param>
        /// <param name="append">
        /// If false the file will be overwritten if it already exists.
        /// If true the contents will be appended to the file.
        /// </param>
        public static void WriteToXmlFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var streamWriter = new StreamWriter(filePath, append))
            {
                xmlSerializer.Serialize(streamWriter, objectToWrite);
            }
        }

        /// <summary>
        /// Reads an object instance from an XML file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// </summary>
        /// <typeparam name="T">The type of object to read from the file.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the XML file.</returns>
        public static T ReadFromXmlFile<T>(string filePath) where T : new()
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var streamReader = new StreamReader(filePath))
            {
                return (T)xmlSerializer.Deserialize(streamReader);
            }
        }
    }
}
