﻿namespace NTEC.Xml.Test
{
    using Domain;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NTEC.Test;
    using System.IO;

    [TestClass]
    public class Serialize : TestBase
    {
        [TestMethod]
        public void Serialize_Object_To_XmlFile()
        {
            var foo = new Foo
            {
                Key = "FooKey",
                Value = "FooValue"
            };
            NTEC.Xml.Serialization.WriteToXmlFile(XML_FILE_PATH, foo);

            var expectedValue = true;
            var actualValue = File.Exists(XML_FILE_PATH);

            Assert.AreEqual(expectedValue, actualValue, "Failed to serialize object to xml file.");
        }

        [TestMethod]
        public void Deserialize_XmlFile_To_Object()
        {
            var foo = NTEC.Xml.Serialization.ReadFromXmlFile<Foo>(XML_FILE_PATH);

            var expectedValue = true;
            var actualValue = foo != null &&
                    !string.IsNullOrEmpty(foo.Key) &&
                    !string.IsNullOrEmpty(foo.Value);

            Assert.AreEqual(expectedValue, actualValue, "Failed to deserialize xml file to object.");
        }
    }
}
