﻿namespace NTEC
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public static class GenericTools
    {
        #region Public Static Methods
        public static object GetDefaultValue(Type type)
        {
            if (type.IsValueType)
                return Activator.CreateInstance(type);

            return null;
        }
        public static Type GetEntityType<T>()
            where T : IList
        {
            Type entityType = null;

            Type type = Activator.CreateInstance<T>().GetType();
            foreach (Type interfaceType in type.GetInterfaces())
            {
                if (interfaceType.IsGenericType && interfaceType.GetGenericTypeDefinition() == typeof(IList<>))
                {
                    entityType = interfaceType.GetGenericArguments()[0];
                    break;
                }
            }

            return entityType;
        }

        public static T ChangeType<T>(object value)
        {
            var t = typeof(T);

            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return default(T);
                }

                t = Nullable.GetUnderlyingType(t);
            }

            return (T)Convert.ChangeType(value, t);
        }
        public static object ChangeType(object value, Type conversion)
        {
            var t = conversion;

            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }

                t = Nullable.GetUnderlyingType(t);
            }

            return Convert.ChangeType(value, t);
        }

        public static Type GetInterfaceImplementation(Type interfaceType)
        {
            return Generic.InterfaceImplementer.GetImplementation(interfaceType);
        }
        #endregion
    }
}