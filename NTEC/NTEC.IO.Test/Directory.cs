﻿namespace NTEC.IO.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NTEC.Test;
    using System;
    using System.IO;

    [TestClass]
    public class Directory : TestBase
    {
        #region Private Fields
        private DirectoryInfo sourceDirectory = new DirectoryInfo(@"C:\Temp\Recursive\Source");
        private DirectoryInfo destinationDirectory = new DirectoryInfo(@"C:\Temp\Recursive\Destination");
        private DirectoryInfo outputDirectory = new DirectoryInfo(@"C:\Temp\Recursive\Output");
        private DirectoryInfo shareDirectory = new DirectoryInfo($@"\\{SHARE_IPADDRESS}\Share");
        #endregion

        #region Public Methods
        [TestMethod]
        public void Replicate_Directory_With_AccessDate()
        {
            DateTime maxAccessDate = DateTime.Now.AddDays(-3);

            NTEC.IO.Directory.Replicate(sourceDirectory, destinationDirectory, maxAccessDate: maxAccessDate);

            bool expectedValue = true;

            Assert.IsTrue(expectedValue, "Failed to replicate directory with specified access date.");
        }

        [TestMethod]
        public void Replicate_Directory_Only_Text_Files()
        {
            NTEC.IO.Directory.Replicate(sourceDirectory, destinationDirectory, fileSearchPattern: "*.txt");

            bool expectedValue = true;

            Assert.IsTrue(expectedValue, "Failed to replicate text files in directory.");
        }

        [TestMethod]
        public void Replicate_Directory_Without_Recursion_Duplicate_Option()
        {
            NTEC.IO.Directory.Replicate(sourceDirectory, destinationDirectory, copyWithRecursion: false, replicateOption: IO.Directory.ReplicateOption.Duplicate);

            bool expectedValue = true;

            Assert.IsTrue(expectedValue, "Failed to replicate directory without recursion and duplication.");
        }

        [TestMethod]
        public void Replicate_Directory_ReplaceIfSame_Option()
        {
            NTEC.IO.Directory.Replicate(sourceDirectory, destinationDirectory, replicateOption: IO.Directory.ReplicateOption.ReplaceDuplicate);

            bool expectedValue = true;

            Assert.IsTrue(expectedValue, "Failed to replicate directory when replacing a file if it is the same.");
        }

        [TestMethod]
        public void Replicate_Directory_With_Output_And_Ignore()
        {
            NTEC.IO.Directory.Replicate(sourceDirectory, destinationDirectory, outputDirectory, replicateOption: IO.Directory.ReplicateOption.Ignore);

            bool expectedValue = true;

            Assert.IsTrue(expectedValue, "Failed to replicate directory with output directory.");
        }

        [TestMethod]
        public void Replicate_Directory_With_Impersonation()
        {
            string username = "Admin";
            string password = "YWRtaW4xMjM=";
            string domain = "WORKGROUP";

            NTEC.IO.Directory.Replicate(username, System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(password)), domain, sourceDirectory, shareDirectory);

            bool expectedValue = true;

            Assert.IsTrue(expectedValue, "Failed to replicate directory with impersonation.");
        }

        [TestMethod]
        public void Replicate_Directory_With_Ignored_Files()
        {
            string[] ignoredFilesExtension = new string[] { ".txt" };

            NTEC.IO.Directory.Replicate(sourceDirectory, destinationDirectory, ignoredFilesExtensions: ignoredFilesExtension);

            bool expectedValue = true;

            Assert.IsTrue(expectedValue, "Failed to replicate directory with ignored files.");
        }
        #endregion
    }
}