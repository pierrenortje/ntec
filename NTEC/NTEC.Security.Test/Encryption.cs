﻿namespace NTEC.Security.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NTEC.Test;
    using System.Threading.Tasks;

    [TestClass]
    public sealed class Encryption : TestBase
    {
        [TestMethod]
        public void DiffieHellman_Encrypt_And_Decrypt()
        {
            var alice = new Security.Encryption.DiffieHellman();
            var bob = new Security.Encryption.DiffieHellman();

            string secretMessage = "Hello World";
            string expectedValue = "Hello World";

            byte[] encryptedMessage = alice.Encrypt(bob.PublicKey, secretMessage);

            Assert.AreNotEqual(secretMessage, System.Text.Encoding.ASCII.GetString(encryptedMessage), "Encryption failed");

            string actualValue = bob.Decrypt(alice.PublicKey, encryptedMessage, alice.IV);

            Assert.AreEqual(expectedValue, actualValue, "Decryption failed");
        }

        [TestMethod]
        public async Task DiffieHellman_EncryptAsync_And_DecryptAsync()
        {
            var alice = new Security.Encryption.DiffieHellman();
            var bob = new Security.Encryption.DiffieHellman();

            string secretMessage = "Hello World";
            string expectedValue = "Hello World";

            byte[] encryptedMessage = await alice.EncryptAsync(bob.PublicKey, secretMessage);

            Assert.AreNotEqual(secretMessage, System.Text.Encoding.ASCII.GetString(encryptedMessage), "Encryption failed");

            string actualValue = await bob.DecryptAsync(alice.PublicKey, encryptedMessage, alice.IV);

            Assert.AreEqual(expectedValue, actualValue, "Decryption failed");
        }
    }
}