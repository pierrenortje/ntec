﻿namespace NTEC.Net.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NTEC.Test;
    using System.Threading.Tasks;

    [TestClass]
    public class WebRequest : TestBase
    {
        #region Private Static Fields
        private const string GET_URL = "http://www.contoso.com/";
        private const string POST_URL = "http://www.contoso.com/PostAccepter.aspx";
        private const string POST_DATA = "This is a test that posts this string to a Web server.";
        #endregion

        #region Public Methods
        [TestMethod]
        public void Send_GET_Request()
        {
            var webRequest = new Net.WebRequest(GET_URL);
            string response = webRequest.Get();

            Assert.IsTrue(!string.IsNullOrEmpty(response), "Failed to retrieve response.");
        }
        [TestMethod]
        public void Send_POST_Request()
        {
            var webRequest = new Net.WebRequest(POST_URL);
            string response = webRequest.Post(POST_DATA);

            Assert.IsTrue(!string.IsNullOrEmpty(response), "Failed to retrieve response.");
        }
        #endregion

        #region Public Async Methods
        [TestMethod]
        public async Task Send_GET_RequestAsync()
        {
            var webRequest = new Net.WebRequest(GET_URL);
            string response = await webRequest.GetAsync();

            Assert.IsTrue(!string.IsNullOrEmpty(response), "Failed to retrieve response.");
        }
        [TestMethod]
        public async Task Send_POST_RequestAsync()
        {
            var webRequest = new Net.WebRequest(POST_URL);
            string response = await webRequest.PostAsync(POST_DATA);

            Assert.IsTrue(!string.IsNullOrEmpty(response), "Failed to retrieve response.");
        }
        #endregion
    }
}