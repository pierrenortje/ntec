﻿namespace NTEC.Xml.Test.Domain
{
    public class Foo
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}