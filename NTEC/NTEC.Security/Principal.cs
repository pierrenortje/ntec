﻿namespace NTEC.Security
{
    using NTEC.Runtime;
    using System;
    using System.ComponentModel;
    using System.Security.Principal;

    public class Principal : IDisposable
    {
        #region Private Fields
        private IntPtr token;
        private IntPtr tokenDuplicate;
        private WindowsImpersonationContext impersonationContext = null;
        #endregion

        #region Constructor
        public Principal(string username, string password, string domain)
        {
            if (!NativeMethods.LogonUser(username, domain, password, NativeMethods.LogonType.NewCredentials, NativeMethods.LogonProvider.Default, out token))
            {
                throw new Win32Exception();
            }

            try
            {
                if (!NativeMethods.DuplicateToken(token, NativeMethods.SecurityImpersonationLevel.Impersonation, out tokenDuplicate))
                {
                    throw new Win32Exception();
                }

                try
                {
                    impersonationContext = new WindowsIdentity(tokenDuplicate).Impersonate();
                }
                finally
                {
                    if (tokenDuplicate != IntPtr.Zero)
                    {
                        if (!NativeMethods.CloseHandle(tokenDuplicate))
                        {
                            throw new Win32Exception();
                        }
                    }
                }
            }
            finally
            {
                if (token != IntPtr.Zero)
                {
                    if (!NativeMethods.CloseHandle(token))
                    {
                        throw new Win32Exception();
                    }
                }
            }
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            if (impersonationContext != null)
            {
                impersonationContext.Undo();
                impersonationContext.Dispose();
                impersonationContext = null;
            }
        }
        #endregion
    }
}