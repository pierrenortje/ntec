﻿namespace NTEC.IO
{
    using Security;
    using System;
    using System.IO;
    using System.Linq;

    public static class Directory
    {
        #region Public Enums
        public enum ReplicateOption
        {
            /// <summary>
            /// Ignore replication if the file exists within the target directory.
            /// </summary>
            Ignore = 1,
            /// <summary>
            /// Copy and replace the file, even if it exists within the target directory.
            /// </summary>
            Replace = 2,
            /// <summary>
            /// Replace the file if it exists within the target directory and
            /// is exactly the same file, else create a duplicate of the file.
            /// If it does not exist, just copy the file.
            /// </summary>
            ReplaceDuplicate = 3,
            /// <summary>
            /// Duplicate the file if it already exists within the target directory,
            /// otherwise just copy the file.
            /// </summary>
            Duplicate = 4
        }
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Recursively replicates a directory and all its subfolders and files with impersonation.
        /// </summary>
        /// <param name="username">The username to use for impersonation.</param>
        /// <param name="password">The password to use for impersonation.</param>
        /// <param name="domain">The domain to use for impersonation.</param>
        /// <param name="sourceDirectory">The directory to replicate from.</param>
        /// <param name="targetDirectory">The directory to replicate to.</param>
        /// <param name="outputDirectory">The alternative directory to output all files replicated between the source and target directories.</param>
        /// <param name="maxAccessDate">The maximum date a file had to be accessed to be replicated.</param>
        /// <param name="fileSearchPattern">The file pattern to search for. Searches for all files by default.</param>
        /// <param name="replicateEmptyFolders">Indicate whether folders with no files should be replicated.</param>
        /// <param name="copyWithRecursion">true to recursively replicate all files and folders, otherwise false to replicate without recursion.</param>
        /// <param name="ignoredFileExtension">An array of file extensions to ignore when replicating.</param>
        public static void Replicate(
                string username,
                string password,
                string domain,
                DirectoryInfo sourceDirectory,
                DirectoryInfo targetDirectory,
                DirectoryInfo outputDirectory = null,
                DateTime? maxAccessDate = null,
                string fileSearchPattern = "*",
                bool replicateEmptyFolders = false,
                bool copyWithRecursion = true,
                ReplicateOption replicateOption = ReplicateOption.ReplaceDuplicate,
                string[] ignoredFileExtension = null)
        {
            using (var pricipal = new Principal(username, password, domain))
            {
                Replicate(sourceDirectory, targetDirectory, outputDirectory, maxAccessDate, fileSearchPattern, replicateEmptyFolders, copyWithRecursion, replicateOption, ignoredFileExtension);
            }
        }
        /// <summary>
        /// Recursively replicates a directory and all its subfolders and files.
        /// </summary>
        /// <param name="sourceDirectory">The directory to replicate from.</param>
        /// <param name="targetDirectory">The directory to replicate to.</param>
        /// <param name="outputDirectory">The alternative directory to output all files replicated between the source and target directories.</param>
        /// <param name="maxAccessDate">The maximum date a file had to be accessed to be replicated.</param>
        /// <param name="fileSearchPattern">The file pattern to search for. Searches for all files by default.</param>
        /// <param name="replicateEmptyFolders">Indicate whether folders with no files should be replicated.</param>
        /// <param name="copyWithRecursion">true to recursively replicate all files and folders, otherwise false to replicate without recursion.</param>
        /// <param name="ignoredFilesExtensions">An array of file extensions to ignore when replicating.</param>
        public static void Replicate(
                DirectoryInfo sourceDirectory,
                DirectoryInfo targetDirectory,
                DirectoryInfo outputDirectory = null,
                DateTime? maxAccessDate = null,
                string fileSearchPattern = "*",
                bool replicateEmptyFolders = false,
                bool copyWithRecursion = true,
                ReplicateOption replicateOption = ReplicateOption.ReplaceDuplicate,
                string[] ignoredFilesExtensions = null)
        {
            maxAccessDate = maxAccessDate ?? DateTime.MinValue;

            if (!targetDirectory.Exists)
                targetDirectory.Create();

            if (outputDirectory != null && !outputDirectory.Exists)
                outputDirectory.Create();

            FileInfo[] sourceDirectoryFiles = null;
            if (copyWithRecursion)
            {
                sourceDirectoryFiles = sourceDirectory.GetFiles(fileSearchPattern);
            }
            else
            {
                sourceDirectoryFiles = sourceDirectory.GetFiles(fileSearchPattern, SearchOption.AllDirectories);
            }

            for (int i = 0; i < sourceDirectoryFiles.Length; i++)
            {
                if (sourceDirectoryFiles[i].LastAccessTime <= maxAccessDate)
                    continue;

                if (ignoredFilesExtensions != null && ignoredFilesExtensions.Any(sourceDirectoryFiles[i].Extension.Contains))
                    continue;

                string outputFilePath = null;
                var newFilePath = new FileInfo(Path.Combine(targetDirectory.FullName, sourceDirectoryFiles[i].Name));
                if (outputDirectory != null)
                    outputFilePath = Path.Combine(outputDirectory.FullName, Path.GetFileName(newFilePath.FullName));

                bool fileExists = System.IO.File.Exists(newFilePath.FullName);
                switch (replicateOption)
                {
                    case ReplicateOption.Ignore:
                        if (!fileExists)
                        {
                            sourceDirectoryFiles[i].CopyTo(newFilePath.FullName);
                            if (outputDirectory != null)
                                sourceDirectoryFiles[i].CopyTo(outputFilePath);
                        }
                        break;

                    case ReplicateOption.Replace:
                        sourceDirectoryFiles[i].CopyTo(newFilePath.FullName, overwrite: true);
                        if (outputDirectory != null)
                            sourceDirectoryFiles[i].CopyTo(outputFilePath, overwrite: true);
                        break;

                    case ReplicateOption.ReplaceDuplicate:
                        if (fileExists)
                        {
                            if (!File.Compare(sourceDirectoryFiles[i].FullName, newFilePath.FullName))
                                goto case ReplicateOption.Duplicate;
                            else
                                goto case ReplicateOption.Replace;
                        }
                        else
                        {
                            goto case ReplicateOption.Ignore;
                        }

                    case ReplicateOption.Duplicate:
                        if (!fileExists)
                        {
                            goto case ReplicateOption.Replace;
                        }
                        else
                        {
                            string uniqueFileName = File.GetIncrementalFileName(newFilePath.FullName);
                            sourceDirectoryFiles[i].CopyTo(uniqueFileName);

                            if (outputDirectory != null)
                                sourceDirectoryFiles[i].CopyTo(Path.Combine(outputDirectory.FullName, Path.GetFileName(uniqueFileName)));
                        }
                        break;
                }
            }

            if (!copyWithRecursion)
                return;

            var sourceDirectoryFolders = sourceDirectory.GetDirectories("*");
            for (int i = 0; i < sourceDirectoryFolders.Length; i++)
            {
                var workingDirectory = new DirectoryInfo(Path.Combine(targetDirectory.FullName, sourceDirectoryFolders[i].Name));

                if (outputDirectory != null)
                    outputDirectory = new DirectoryInfo(Path.Combine(outputDirectory.FullName, sourceDirectoryFolders[i].Name));

                Replicate(sourceDirectoryFolders[i], workingDirectory, outputDirectory, maxAccessDate, fileSearchPattern, replicateEmptyFolders, copyWithRecursion, replicateOption, ignoredFilesExtensions);

                if (!replicateEmptyFolders)
                {
                    if (System.IO.Directory.GetFileSystemEntries(workingDirectory.FullName).Length == 0)
                    {
                        System.IO.Directory.Delete(workingDirectory.FullName);
                    }
                }

                if (outputDirectory != null)
                {
                    if (System.IO.Directory.GetFileSystemEntries(outputDirectory.FullName).Length == 0)
                    {
                        System.IO.Directory.Delete(outputDirectory.FullName);
                    }
                }
            }
        }
        #endregion
    }
}