﻿using System.Security.Cryptography;
using System.Text;

namespace NTEC.NET6.Cryptography
{
    public static class RSA
    {
        // Encrypt data using the public key
        public static byte[] EncryptData(string plainText, string publicKey)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(publicKey); // Load public key
                byte[] dataToEncrypt = Encoding.UTF8.GetBytes(plainText);
                return rsa.Encrypt(dataToEncrypt, false); // Encrypt using PKCS#1 v1.5 padding
            }
        }

        // Decrypt data using the private key
        public static string DecryptData(byte[] encryptedData, string privateKey)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(privateKey); // Load private key
                byte[] decryptedData = rsa.Decrypt(encryptedData, false); // Decrypt using PKCS#1 v1.5 padding
                return Encoding.UTF8.GetString(decryptedData);
            }
        }
    }
}
