﻿namespace NTEC.IO
{
    using System;
    using System.IO;
    using System.IO.Compression;
    using System.Threading.Tasks;

    public static class File
    {
        #region Private Static Fields
        private const string COMPRESSED_FILE_EXTENSION = ".gz";
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Compresses all files within a directory.
        /// </summary>
        /// <param name="directory">The directory to compress files.</param>
        public static void CompressDirectory(DirectoryInfo directory)
        {
            foreach (var fileToCompress in directory.GetFiles())
            {
                Compress(fileToCompress);
            }
        }
        /// <summary>
        /// Compresses all files within a directory as an asynchronous operation.
        /// </summary>
        /// <param name="directory">The directory to compress files.</param>
        public static async Task CompressDirectoryAsync(DirectoryInfo directory)
        {
            foreach (var fileToCompress in directory.GetFiles())
            {
                await CompressAsync(fileToCompress);
            }
        }

        /// <summary>
        /// Creates a new compressed file.
        /// </summary>
        /// <param name="file">The file to compress.</param>
        /// <returns>The compressed file's information.</returns>
        public static FileInfo Compress(FileInfo file)
        {
            FileInfo outputFile = null;

            using (var originalFileStream = file.OpenRead())
            {
                if ((System.IO.File.GetAttributes(file.FullName)
                    & FileAttributes.Hidden) != FileAttributes.Hidden & file.Extension != COMPRESSED_FILE_EXTENSION)
                {
                    string newFileName = file.FullName + COMPRESSED_FILE_EXTENSION;
                    using (var compressedFileStream = System.IO.File.Create(newFileName))
                    {
                        using (var compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
                        {
                            originalFileStream.CopyTo(compressionStream);
                        }
                    }

                    outputFile = new FileInfo(newFileName);
                }
            }

            return outputFile;
        }
        /// <summary>
        /// Creates a new compressed file as an asynchronous operation.
        /// </summary>
        /// <param name="file">The file to compress.</param>
        /// <returns>The compressed file's information.</returns>
        public static async Task<FileInfo> CompressAsync(FileInfo file)
        {
            FileInfo outputFile = null;

            using (var originalFileStream = file.OpenRead())
            {
                if ((System.IO.File.GetAttributes(file.FullName) & FileAttributes.Hidden)
                    != FileAttributes.Hidden & file.Extension != COMPRESSED_FILE_EXTENSION)
                {
                    string newFileName = file.FullName + COMPRESSED_FILE_EXTENSION;
                    using (var compressedFileStream = System.IO.File.Create(newFileName))
                    {
                        using (var compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
                        {
                            await originalFileStream.CopyToAsync(compressionStream);
                        }
                    }

                    outputFile = new FileInfo(newFileName);
                }
            }

            return outputFile;
        }

        /// <summary>
        /// Decompresses all files within a directory.
        /// </summary>
        /// <param name="directory">The directory to decompress files.</param>
        public static void DecompressDirectory(DirectoryInfo directory)
        {
            foreach (var fileToCompress in directory.GetFiles())
            {
                Decompress(fileToCompress);
            }
        }
        /// <summary>
        /// Decompresses all files within a directory as an asynchronous operation.
        /// </summary>
        /// <param name="directory">The directory to decompress files.</param>
        public static async Task DecompressDirectoryAsync(DirectoryInfo directory)
        {
            foreach (var fileToCompress in directory.GetFiles())
            {
                await DecompressAsync(fileToCompress);
            }
        }

        /// <summary>
        /// Creates a new decompressed file.
        /// </summary>
        /// <param name="file">The file to decompress.</param>
        /// <returns>The decompressed file's information.</returns>
        public static FileInfo Decompress(FileInfo file)
        {
            FileInfo outputFile = null;

            using (var originalFileStream = file.OpenRead())
            {
                string currentFileName = file.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - file.Extension.Length);

                using (var decompressedFileStream = System.IO.File.Create(newFileName))
                {
                    using (var decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                    }
                }

                outputFile = new FileInfo(newFileName);
            }

            return outputFile;
        }
        /// <summary>
        /// Creates a new decompressed file as an asynchronous operation.
        /// </summary>
        /// <param name="file">The file to decompress.</param>
        /// <returns>The decompressed file's information.</returns>
        public static async Task<FileInfo> DecompressAsync(FileInfo file)
        {
            FileInfo outputFile = null;

            using (var originalFileStream = file.OpenRead())
            {
                string currentFileName = file.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - file.Extension.Length);

                using (FileStream decompressedFileStream = System.IO.File.Create(newFileName))
                {
                    using (var decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        await decompressionStream.CopyToAsync(decompressedFileStream);
                    }
                }

                outputFile = new FileInfo(newFileName);
            }

            return outputFile;
        }

        /// <summary>
        /// Compares two files to determine if they are the same.
        /// </summary>
        /// <param name="filePath1">The first file to compare.</param>
        /// <param name="filePath2">The second file to compare.</param>
        /// <returns>True if the two files are the same, otherwise false.</returns>
        public static bool Compare(string filePath1, string filePath2)
        {
            return Compare(new FileInfo(filePath1), new FileInfo(filePath2));
        }
        /// <summary>
        /// Compares two files to determine if they are the same file.
        /// </summary>
        /// <param name="file1">The first file to compare.</param>
        /// <param name="file2">The second file to compare.</param>
        /// <returns>true if the two files are the same, otherwise false.</returns>
        public static bool Compare(FileInfo file1, FileInfo file2)
        {
            int file1Byte;
            int file2Byte;
            FileStream fileStream1 = null;
            FileStream fileStream2 = null;

            try
            {
                if (file1 == file2)
                {
                    return true;
                }

                fileStream1 = new FileStream(file1.FullName, FileMode.Open, FileAccess.Read);
                fileStream2 = new FileStream(file2.FullName, FileMode.Open, FileAccess.Read);

                if (fileStream1.Length != fileStream2.Length)
                {
                    return false;
                }

                do
                {
                    file1Byte = fileStream1.ReadByte();
                    file2Byte = fileStream2.ReadByte();
                }
                while ((file1Byte == file2Byte) && (file1Byte != -1));

                return ((file1Byte - file2Byte) == 0);
            }
            finally
            {
                if (fileStream1 != null)
                {
                    fileStream1.Close();
                }

                if (fileStream2 != null)
                {
                    fileStream2.Close();
                }
            }
        }

        /// <summary>
        /// Generate a file name post fixed with an incremental number, if not
        /// unique, incrementing the number until a unique file name is found.
        /// </summary>
        /// <param name="filePath">The path to the file.</param>
        /// <returns>A file name, if not unique, ending with a number, else the original file name.</returns>
        public static string GetIncrementalFileName(string filePath)
        {
            return GetIncrementalFileName(new FileInfo(filePath));
        }
        /// <summary>
        /// Generate a file name post fixed with an incremental number, if not
        /// unique, incrementing the number until a unique file name is found.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>A file name, if not unique, ending with a number, else the original file name.</returns>
        public static string GetIncrementalFileName(FileInfo file)
        {
            string newFilePath = file.FullName;
            bool fileExists = System.IO.File.Exists(newFilePath);
            if (fileExists)
            {
                int count = 1;
                while (fileExists)
                {
                    string fileNameOnly = Path.GetFileNameWithoutExtension(file.FullName);
                    string extension = Path.GetExtension(file.FullName);
                    string path = Path.GetDirectoryName(file.FullName);
                    string newFileName = $"{fileNameOnly} ({count++})";

                    newFilePath = Path.Combine(path, $"{newFileName}{extension}");
                    fileExists = System.IO.File.Exists(newFilePath);
                }
            }

            return newFilePath;
        }

        /// <summary>
        /// Returns the file's hashed value.
        /// </summary>
        /// <param name="filePath">The path to the file.</param>
        /// <returns>A string representing the hashed value of the file.</returns>
        public static string GetHash(string filePath)
        {
            return GetHash(new FileInfo(filePath));
        }
        /// <summary>
        /// Returns the file's hashed value.
        /// </summary>
        /// <param name="file">The specified file.</param>
        /// <returns>A string representing the hashed value of the file.</returns>
        public static string GetHash(FileInfo file)
        {
            string fileHash = null;
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                using (var fileStream = System.IO.File.OpenRead(file.FullName))
                {
                    fileHash = BitConverter.ToString(md5.ComputeHash(fileStream)).Replace("-", "‌​").ToLower();
                }
            }
            return fileHash;
        }
        #endregion
    }
}