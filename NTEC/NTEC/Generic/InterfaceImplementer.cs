﻿namespace NTEC.Generic
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Reflection.Emit;

    /// <summary>
    /// A class used to implement an interface at runtime.
    /// </summary>
    public static class InterfaceImplementer
    {
        #region Private Static Fields
        private static readonly Dictionary<Type, Type> cache = null;
        private static readonly AssemblyBuilder assemblyBuilder = null;
        private static readonly ModuleBuilder moduleBuilder = null;
        #endregion

        #region Static Constructor
        static InterfaceImplementer()
        {
            cache = new Dictionary<Type, Type>();
            assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName("InterfaceImplementer"), AssemblyBuilderAccess.Run);
            moduleBuilder = assemblyBuilder.DefineDynamicModule("InterfaceImplementer");
        }
        #endregion

        #region Private Static Methods
        private static Type CreateImplementation(Type interfaceType)
        {
            return ImplementDataInterface("Imp_" + interfaceType.Name, interfaceType).CreateType();
        }
        private static TypeBuilder ImplementDataInterface(string className, Type interfaceType)
        {
            var typeBuilder = moduleBuilder.DefineType(className
                , TypeAttributes.Public | TypeAttributes.AutoClass | TypeAttributes.AnsiClass | TypeAttributes.BeforeFieldInit
                , typeof(System.Object)
                , new[] { interfaceType });

            var properties = new List<PropertyInfo>();
            GetInterfaceProperties(properties, interfaceType);

            foreach (var propertyTypeInfo in properties)
                ImplemenentProperty(typeBuilder, propertyTypeInfo.PropertyType, propertyTypeInfo.Name);

            return typeBuilder;
        }
        private static void ImplemenentProperty(TypeBuilder typeBuilder, Type type, string name)
        {
            var field = typeBuilder.DefineField("_" + name, type, FieldAttributes.Private);
            var property = typeBuilder.DefineProperty(name, PropertyAttributes.HasDefault, type, null);

            var getter = typeBuilder.DefineMethod("get_" + name,
             MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual,
             type, Type.EmptyTypes);
            {
                var il = getter.GetILGenerator();
                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldfld, field);
                il.Emit(OpCodes.Ret);
            }

            var setter = typeBuilder.DefineMethod("set_" + name,
             MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual,
             null, new[] { type });
            {
                var il = setter.GetILGenerator();
                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldarg_1);
                il.Emit(OpCodes.Stfld, field);
                il.Emit(OpCodes.Ret);
            }

            property.SetGetMethod(getter);
            property.SetSetMethod(setter);
        }
        private static void GetInterfaceProperties(List<PropertyInfo> properties, Type interfaceType)
        {
            foreach (var property in interfaceType.GetProperties())
            {
                if (properties.Exists(p => p.Name == property.Name))
                    continue;

                properties.Add(property);
            }

            foreach (var subInterface in interfaceType.GetInterfaces())
                GetInterfaceProperties(properties, subInterface);
        }
        #endregion

        #region Public Static Methods
        public static Type GetImplementation(Type interfaceType)
        {
            Type implementation;
            lock (cache)
            {
                if (!cache.TryGetValue(interfaceType, out implementation))
                {
                    implementation = CreateImplementation(interfaceType);
                    cache[interfaceType] = implementation;
                }
            }
            return implementation;
        }
        #endregion
    }
}