﻿namespace NTEC.IO.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NTEC.Test;
    using System.IO;
    using System.Threading.Tasks;

    [TestClass]
    public class File : TestBase
    {
        #region Private Static Fields
        private const string XML_FILE_PATH_COMPRESSED = TEMP_PATH + "Unit_Test_File.xml.gz";
        #endregion

        #region Public Methods
        [TestMethod]
        public void Compress_File()
        {
            FileInfo file = new FileInfo(XML_FILE_PATH);
            FileInfo compressedFile = NTEC.IO.File.Compress(file);

            // Check to see if the file has been compressed
            bool actualValue = compressedFile.Length < file.Length;

            Assert.IsTrue(actualValue, "Failed to compress file.");
        }

        [TestMethod]
        public async Task CompressAsync_File()
        {
            FileInfo file = new FileInfo(XML_FILE_PATH);
            FileInfo compressedFile = await NTEC.IO.File.CompressAsync(file);

            // Check to see if the file has been compressed
            bool actualValue = compressedFile.Length < file.Length;

            Assert.IsTrue(actualValue, "Failed to compress file.");
        }

        [TestMethod]
        public void Decompress_File()
        {
            FileInfo file = new FileInfo(XML_FILE_PATH_COMPRESSED);
            FileInfo uncompressedFile = NTEC.IO.File.Decompress(file);

            // Check to see if the file has been compressed
            bool actualValue = uncompressedFile.Length > file.Length;

            Assert.IsTrue(actualValue, "Failed to decompress file.");
        }

        [TestMethod]
        public async Task DecompressAsync_File()
        {
            FileInfo file = new FileInfo(XML_FILE_PATH_COMPRESSED);
            FileInfo uncompressedFile = await NTEC.IO.File.DecompressAsync(file);

            // Check to see if the file has been compressed
            bool actualValue = uncompressedFile.Length > file.Length;

            Assert.IsTrue(actualValue, "Failed to decompress file.");
        }

        [TestMethod]
        public void Compare_Unique_Files()
        {
            string file1 = TEMP_PATH + "testCompare1.txt";
            string file2 = TEMP_PATH + "testCompare2.txt";

            var fileStream1 = System.IO.File.Create(file1);
            fileStream1.Close();

            var fileStream2 = System.IO.File.Create(file2);
            fileStream2.Close();

            System.IO.File.WriteAllText(file1, "Hello World");
            System.IO.File.WriteAllText(file2, "Hello World");

            bool actualValue = NTEC.IO.File.Compare(file1, file2);

            Assert.IsTrue(actualValue, "Expected files to be the same.");
        }

        [TestMethod]
        public void Compare_Non_Unique_Files()
        {
            string file1 = TEMP_PATH + "testCompare1.txt";
            string file2 = TEMP_PATH + "testCompare2.txt";

            var fileStream1 = System.IO.File.Create(file1);
            fileStream1.Close();

            var fileStream2 = System.IO.File.Create(file2);
            fileStream2.Close();

            System.IO.File.WriteAllText(file1, "Hello World");
            System.IO.File.WriteAllText(file2, "Goodbye World");

            bool actualValue = NTEC.IO.File.Compare(file1, file2);

            Assert.IsFalse(actualValue, "Expected files to be different.");
        }
        #endregion
    }
}