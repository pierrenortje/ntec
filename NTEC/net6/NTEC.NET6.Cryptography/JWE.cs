﻿namespace NTEC.NET6.Cryptography
{
    using Microsoft.IdentityModel.Tokens;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Security.Cryptography.X509Certificates;

    public static class JWE
    {
        public static string Encrypt(IEnumerable<Claim> claims, SecurityKey publicKey, SigningCredentials signingCredentials)
        {
            return Encrypt(claims, publicKey, signingCredentials, null, null);
        }
        public static string Encrypt(IEnumerable<Claim> claims, SecurityKey publicKey, SigningCredentials signingCredentials, string? issuer, string? audience)
        {
            // Create a token descriptor
            var securityTokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = issuer,
                Audience = audience,
                SigningCredentials = signingCredentials,
                EncryptingCredentials = new EncryptingCredentials(
                    publicKey,
                    SecurityAlgorithms.RsaOAEP,
                    SecurityAlgorithms.Aes256CbcHmacSha512), // Content encryption algorithm
                Claims = new JwtPayload(claims)
            };

            // Generate the token
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateJwtSecurityToken(securityTokenDescriptor);

            // Write the encrypted token
            return tokenHandler.WriteToken(token);
        }

        public static string Decrypt(string encryptedJwe, SecurityKey publicKey, SecurityKey privateKey)
        {
            return Decrypt(encryptedJwe, publicKey, privateKey, null, null);
        }
        public static string Decrypt(string encryptedJwe, SecurityKey publicKey, SecurityKey privateKey, string? validIssuer, string? validAudience)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            // Token validation parameters
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidIssuer = validIssuer,
                ValidateIssuer = validIssuer != null,
                ValidAudience = validAudience,
                ValidateAudience = validAudience != null,

                RequireSignedTokens = true,
                IssuerSigningKey = publicKey,
                TokenDecryptionKey = privateKey,
            };

            // Validate and decrypt
            tokenHandler.ValidateToken(encryptedJwe, tokenValidationParameters, out SecurityToken validatedToken);

            // Extract and return the payload
            return ((JwtSecurityToken)validatedToken).Payload.SerializeToJson();
        }
    }
}
