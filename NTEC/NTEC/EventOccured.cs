﻿namespace NTEC
{
    using System;
    using System.Linq;

    #region Public Enums
    public enum EventType
    {
        Info,
        Warning,
        Error
    }
    #endregion
    
    #region Public Classes
    public class EventOccuredEventArgs : EventArgs
    {
        #region Public Constructor
        public EventOccuredEventArgs(string message, EventType type = EventType.Info)
        {
            this.Type = type;
            this.Message = message;
        }
        #endregion

        #region Public Properties
        public EventType Type { get; set; }
        public string Message { get; set; }
        #endregion
    }

    public abstract class EventOccuredBase : IDisposable
    {
        #region Protected Fields
        protected string[] warningMessages;
        protected string[] errorMessages;
        #endregion

        #region Protected Methods
        protected void OnEventOccured(string text)
        {
            if (errorMessages != null && warningMessages != null)
            {
                EventOccured?.Invoke(this,
                    new EventOccuredEventArgs(text, errorMessages.Any(s => text.Contains(s)) ? EventType.Error :
                    warningMessages.Any(s => text.Contains(s)) ? EventType.Warning : EventType.Info));
            }
            else if (warningMessages != null)
            {
                EventOccured?.Invoke(this,
                    new EventOccuredEventArgs(text, warningMessages.Any(s => text.Contains(s)) ? EventType.Warning : EventType.Info));
            }
            else if (errorMessages != null)
            {
                EventOccured?.Invoke(this,
                    new EventOccuredEventArgs(text, errorMessages.Any(s => text.Contains(s)) ? EventType.Error : EventType.Info));
            }
            else
            {
                EventOccured?.Invoke(this,
                    new EventOccuredEventArgs(text, EventType.Info));
            }
        }
        protected void OnEventOccured(string text, EventType type)
        {
            EventOccured?.Invoke(this, new EventOccuredEventArgs(text, type));
        }
        #endregion

        #region Public Events
        public event EventOccuredEventHandler EventOccured;
        #endregion

        #region IDisposable Members
        public virtual void Dispose() { }
        #endregion
    }
    #endregion

    #region Public Delegates
    public delegate void EventOccuredEventHandler(object sender, EventOccuredEventArgs e);
    #endregion
}