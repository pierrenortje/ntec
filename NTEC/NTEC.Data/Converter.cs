﻿namespace NTEC.Data
{
    using NTEC.Generic;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Reflection;

    public static class Converter
    {
        #region Public Static Methods
        /// <summary>
        /// Populates an Entity List with data from the DataTable.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="list">The destination list to populate.</param>
        /// <param name="dataTable">The source data table used to populate the list with.</param>
        public static void PopulateList<T>(T list, DataTable dataTable)
            where T : IList
        {
            if (dataTable == null)
                return;

            PopulateList<T>(list, dataTable.Select());
        }
        /// <summary>
        /// Populates an Entity List with data from the DataRow array.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="list">The destination list to populate.</param>
        /// <param name="dataRows">The source data rows used to populate the list with.</param>
        public static void PopulateList<T>(T list, DataRow[] dataRows)
            where T : IList
        {
            if (dataRows == null)
                return;

            if (list == null)
                list = Activator.CreateInstance<T>();

            Type entityType = GenericTools.GetEntityType<T>();
            if (entityType == null)
                return;

            for (int i = 0; i < dataRows.Length; i++)
                list.Add(ToEntity(dataRows[i], entityType));
        }

        /// <summary>
        /// Populates an array with data from the objects array.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="array">The destination array to populate.</param>
        /// <param name="dataTable">The dataTable used to populate the array with.</param>
        public static void PopulateArray<T>(T[] array, DataTable dataTable)
        {
            if (array == null)
                return;

            PopulateArray<T>(array, dataTable.Select());
        }
        /// <summary>
        /// Populates an array with data from the objects array.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="array">The destination array to populate.</param>
        /// <param name="dataRows">The dataRows used to populate the array with.</param>
        public static void PopulateArray<T>(T[] array, DataRow[] dataRows)
        {
            if (array == null)
                return;

            if (array == null)
                array = (T[])Array.CreateInstance(typeof(T), dataRows.Length);

            for (int i = 0; i < dataRows.Length; i++)
                array[i] = ToEntity<T>(dataRows[i]);
        }

        /// <summary>
        /// Populates an Entity with data from the DataRow.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="entity">The destination entity to populate.</param>
        /// <param name="dataRow">The source data row used to populate the entity with.</param>
        public static void PopulateEntity<T>(T entity, DataRow dataRow)
        {
            Type type = entity.GetType();
            foreach (PropertyInfo property in type.GetProperties())
            {
                if (!dataRow.Table.Columns.Contains(property.Name))
                    continue;

                object columnValue = dataRow[property.Name];
                if (columnValue == DBNull.Value)
                    continue;

                Type propertyType = property.PropertyType;
                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    propertyType = Nullable.GetUnderlyingType(propertyType);

                columnValue = Convert.ChangeType(columnValue, propertyType);

                property.SetValue(entity, columnValue, null);
            }
        }
        /// <summary>
        /// Populates an Entity with data from the Object.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="entity">The destination entity to populate.</param>
        /// <param name="obj">The source object used to populate the entity with.</param>
        public static void PopulateEntity<T>(T entity, object obj)
        {
            NTEC.Generic.Converter.PopulateEntity<T>(entity, obj);
        }

        /// <summary>
        /// Converts a DataRow to an given Entity type using column names to map to matching properties in the entity.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="dataRow">The source data row containg the data used to populate the entity's properties with.</param>
        /// <returns></returns>
        public static T ToEntity<T>(DataRow dataRow)
        {
            T entity = default(T);
            if (dataRow == null)
                return entity;

            entity = Activator.CreateInstance<T>();
            PopulateEntity<T>(entity, dataRow);

            return entity;
        }
        /// <summary>
        /// Converts a DataRow to an given Entity type using column names to map to matching properties in the entity.
        /// </summary>
        /// <param name="dataRow">The source data row containg the data used to populate the entity's properties with.</param>
        /// <param name="entityType">The type of entity to create and populate from the object.</param>
        /// <returns></returns>
        public static object ToEntity(DataRow dataRow, Type entityType)
        {
            object entity = GenericTools.GetDefaultValue(entityType);
            if (dataRow == null)
                return entity;

            if (entityType.IsInterface)
                entityType = InterfaceImplementer.GetImplementation(entityType);

            entity = Activator.CreateInstance(entityType);
            PopulateEntity(entity, dataRow);

            return entity;
        }

        /// <summary>
        /// Converts a DataTable to an Entity List.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="dataTable">The source data table containing the data used to populate the list with.</param>
        /// <returns></returns>
        public static T ToList<T>(DataTable dataTable)
            where T : IList
        {
            if (dataTable == null)
                return default(T);

            return ToList<T>(dataTable.Select());
        }
        /// <summary>
        /// Converts a DataRow array to an Entity List.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="dataRows">The source data rows containing the data used to populate the list with.</param>
        /// <returns></returns>
        public static T ToList<T>(DataRow[] dataRows)
            where T : IList
        {
            if (dataRows == null)
                return default(T);

            T list = Activator.CreateInstance<T>();
            PopulateList<T>(list, dataRows);

            return list;
        }

        /// <summary>
        /// Converts a DataTable to an array.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="dataTable">The source data table containing the data used to populate the array with.</param>
        /// <returns></returns>
        public static T[] ToArray<T>(DataTable dataTable)
        {
            if (dataTable == null)
                return null;

            return ToArray<T>(dataTable.Select());
        }
        /// <summary>
        /// Converts a DataRow array to an Entity List.
        /// </summary>
        /// <typeparam name="T">The type of the destination entity.</typeparam>
        /// <param name="dataRows">The source data rows containing the data used to populate the array with.</param>
        /// <returns></returns>
        public static T[] ToArray<T>(DataRow[] dataRows)
        {
            if (dataRows == null)
                return null;

            T[] array = (T[])Array.CreateInstance(typeof(T), dataRows.Length);
            PopulateArray<T>(array, dataRows);

            return array;
        }

        /// <summary>
        /// Convert the entity list to a empty DataTable with matching columns for each property.
        /// </summary>
        /// <typeparam name="T">The type of entity used to create the DataTable schema from.</typeparam>
        /// <returns>The created data table.</returns>
        public static DataTable ToDataTable<T>()
            where T : IList
        {
            Type entityType = GenericTools.GetEntityType<T>();
            var dataTable = new DataTable(entityType.Name);

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);
            foreach (PropertyDescriptor prop in properties)
                dataTable.Columns.Add(prop.Name, prop.PropertyType);

            return dataTable;
        }
        /// <summary>
        /// Convert the entity list to a populate DataTable with matching columns for each property.
        /// </summary>
        /// <typeparam name="T">The type of entity used to create the DataTable schema from.</typeparam>
        /// <param name="list">The destination list to be populate.</param>
        /// <returns>The populated data table.</returns>
        public static DataTable ToDataTable<T>(T list)
            where T : IList
        {
            DataTable dataTable = ToDataTable<T>();
            Type entityType = GenericTools.GetEntityType<T>();

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);
            foreach (var item in list)
            {
                DataRow dataRow = dataTable.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    dataRow[prop.Name] = prop.GetValue(item);

                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }
        #endregion
    }
}