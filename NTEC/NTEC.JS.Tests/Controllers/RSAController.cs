﻿using Microsoft.AspNetCore.Mvc;
using NTEC.JS.Tests.Models;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace NTEC.JS.Tests.Controllers
{
    [ApiController]
    [Route("api/rsa-encrypt")]
    public class RSAController : ControllerBase
    {
        #region Private Fields
        private static RSAParameters _publicKey;
        private static RSAParameters _privateKey;
        #endregion

        #region Static Constructor
        static RSAController()
        {
            // Load the certificate from the current machine's certificate store
            var certificate = Helper.LoadCertificateFromMachineStore(Configuration.CertificateThumbprint);
            var rsa = certificate.GetRSAPrivateKey();
            if (rsa == null)
                throw new Exception("Failed to load certificate.");

            _publicKey = rsa.ExportParameters(false);
            _privateKey = rsa.ExportParameters(true);
        }
        #endregion

        #region Action Results
        [HttpGet("public-key")]
        public IActionResult GetPublicKey()
        {
            string mod = Convert.ToBase64String(_publicKey.Modulus);
            string exp = Convert.ToBase64String(_publicKey.Exponent);

            var jwk = new
            {
                // Key type (RSA)
                kty = "RSA",

                // Public key modulus
                n = mod.Base64ToBase64Url(),

                // Exponent
                e = exp.Base64ToBase64Url(),

                // Signature algorithm
                alg = "RSA-OAEP-256",

                // Key use (enc = encryption i.e. the public key)
                use = "enc"
            };

            return Ok(jwk);
        }

        [HttpPost("encrypted-data")]
        public IActionResult GetEncryptedData([FromBody] EncryptedContentModel data)
        {
            try
            {
                using (var rsa = RSA.Create())
                {
                    // Import our private key before we can decrypt
                    rsa.ImportParameters(_privateKey);

                    // Convert the body content to bytes
                    byte[] encBytes = Convert.FromBase64String(data.Content);

                    // And decrypt it using RSA
                    byte[] encryptedData = rsa.Decrypt(encBytes, RSAEncryptionPadding.OaepSHA256);

                    // Convert the bytes back to a string
                    string clientPK = Encoding.UTF8.GetString(encryptedData);

                    // And convert the base64Url to base64 format
                    string publicKey = clientPK.Base64UrlToBase64();

                    // The secret content
                    string secretMessage = "Keep it secret! Keep it safe!";

                    // Perform the encryption using the symmetric public key the client has sent us
                    var result = Security.Cryptography.NAesGCM.Encrypt(secretMessage, publicKey);

                    // And send them the nonce (IV), tag and encrypted content
                    return Ok(new
                    {
                        nonce = result.Nonce,
                        text = result.EncryptedText,
                        tag = result.Tag,
                    });
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
