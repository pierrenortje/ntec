namespace NTEC.NET6.Cryptography.Test
{
    using NTEC.Security.Cryptography;
    using System.Security.Cryptography;

    [TestClass]
    public class NAesGCMTests
    {
        [TestMethod]
        public void TestAesGCM()
        {
            // Example usage
            string plaintext = "Hello, AES-GCM!!!";
            string key = GenerateKey();
            Console.WriteLine("Key: " + key);

            (string encryptedText, string nonce, string tag) = NAesGCM.Encrypt(plaintext, key);
            Console.WriteLine($"Encrypted: {encryptedText}");
            Console.WriteLine($"Nonce: {nonce}");
            Console.WriteLine($"Tag: {tag}");

            string decryptedText = NAesGCM.Decrypt(encryptedText, nonce, tag, key);
            Console.WriteLine($"Decrypted: {decryptedText}");

            Assert.AreEqual(plaintext, decryptedText);
        }

        private static string GenerateKey()
        {
            byte[] key = new byte[32]; // 256-bit key
            RandomNumberGenerator.Fill(key);
            return Convert.ToBase64String(key);
        }
    }
}