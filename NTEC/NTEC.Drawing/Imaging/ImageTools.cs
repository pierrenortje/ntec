﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace NTEC.Drawing.Imaging
{
    public class ImageTools
    {
        #region Private Static Methods
        private static ImageCodecInfo GetImageCodecInfo(ImageFormat format)
        {
            if (format == null || format.Guid == Guid.Empty)
                return null;

            var imageEncoders = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in imageEncoders)
            {
                if (codec.FormatID == format.Guid)
                    return codec;
            }
            return null;
        }
        private static ImageCodecInfo GetImageCodecInfo(string fileExtension)
        {
            if (string.IsNullOrEmpty(fileExtension))
                return null;

            var imageEncoders = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in imageEncoders)
            {
                if (codec.FilenameExtension.ToLower().Contains(fileExtension.ToLower()))
                    return codec;
            }

            return null;
        }
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Gets an Image Object from the specified fully qualified file name.
        /// </summary>
        /// <param name="fileName">The name of the image file.</param>
        /// <returns></returns>
        public static Image GetImage(string fileName)
        {
            return GetImage(new FileInfo(fileName));
        }
        /// <summary>
        /// Gets an Image Object from the specified file.
        /// </summary>
        /// <param name="fileInfo">The file info to the image.</param>
        /// <returns></returns>
        public static Image GetImage(FileInfo fileInfo)
        {
            if (fileInfo == null)
                throw new NullReferenceException("Unable to load image, null file reference.");

            if (!fileInfo.Exists)
                throw new FileNotFoundException("Unable to load image, File '" + fileInfo.FullName + "' does not exist.", fileInfo.FullName);

            Image image = null;

            try
            {
                using (FileStream fileStream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    image = Image.FromStream(fileStream);

                    // It appears that the memory stream that the object was created on has to be open to use the bitmap, thus creating a new instance.
                    if (image is Bitmap)
                        image = new Bitmap(image);

                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                image = null;

                throw new Exception("Unable to load image, " + fileInfo.FullName + ". " + ex.Message, ex);
            }

            return image;
        }

        /// <summary>
        /// Gets an Image Object from the specified fully qualified file name.
        /// </summary>
        /// <param name="fileName">The name of the image file.</param>
        /// <param name="size">The desired size of the returning image.</param>
        /// <returns></returns>
        public static Image GetImage(string fileName, Size size)
        {
            return GetImage(GetImage(fileName), size);
        }
        /// <summary>
        /// Gets an Image Object from the specified fully qualified file name.
        /// </summary>
        /// <param name="fileName">The name of the image file.</param>
        /// <param name="size">The desired size of the returning image.</param>
        /// <param name="backgroundColor">The color of the background when the image resize does not fit the same dimensions as the original image.</param>
        /// <returns></returns>
        public static Image GetImage(string fileName, Size size, System.Drawing.Color backgroundColor)
        {
            return GetImage(GetImage(fileName), size, backgroundColor);
        }
        /// <summary>
        /// Gets an Image Object from the specified fully qualified file name.
        /// </summary>
        /// <param name="fileName">The name of the image file.</param>
        /// <param name="width">The width of the image.</param>
        /// <param name="height">The height of the image.</param>
        /// <returns></returns>
        public static Image GetImage(string fileName, int width, int height)
        {
            return GetImage(GetImage(fileName), width, height);
        }
        /// <summary>
        /// Gets an Image Object from the specified fully qualified file name.
        /// </summary>
        /// <param name="fileName">The name of the image file.</param>
        /// <param name="width">The width of the image.</param>
        /// <param name="height">The height of the image.</param>
        /// <param name="backgroundColor">The color of the background when the image resize does not fit the same dimensions as the original image.</param>
        /// <returns></returns>
        public static Image GetImage(string fileName, int width, int height, System.Drawing.Color backgroundColor)
        {
            return GetImage(GetImage(fileName), width, height, backgroundColor);
        }

        /// <summary>
        /// Gets an Image Object from the specified file.
        /// </summary>
        /// <param name="fileInfo">The file info to the image.</param>
        /// <param name="size">The desired size of the returning image.</param>
        /// <returns></returns>
        public static Image GetImage(FileInfo fileInfo, Size size)
        {
            return GetImage(GetImage(fileInfo), size);
        }
        /// <summary>
        /// Gets an Image Object from the specified file.
        /// </summary>
        /// <param name="fileInfo">The file info to the image.</param>
        /// <param name="size">The desired size of the returning image.</param>
        /// <param name="backgroundColor">The color of the background when the image resize does not fit the same dimensions as the original image.</param>
        /// <returns></returns>
        public static Image GetImage(FileInfo fileInfo, Size size, System.Drawing.Color backgroundColor)
        {
            return GetImage(GetImage(fileInfo), size, backgroundColor);
        }
        /// <summary>
        /// Gets an Image Object from the specified fully qualified file name.
        /// </summary>
        /// <param name="fileInfo">The file info to the image.</param>
        /// <param name="width">The width of the image.</param>
        /// <param name="height">The height of the image.</param>
        /// <returns></returns>
        public static Image GetImage(FileInfo fileInfo, int width, int height)
        {
            return GetImage(GetImage(fileInfo), width, height);
        }
        /// <summary>
        /// Gets an Image Object from the specified fully qualified file name.
        /// </summary>
        /// <param name="fileInfo">The file info to the image.</param>
        /// <param name="width">The width of the image.</param>
        /// <param name="height">The height of the image.</param>
        /// <param name="backgroundColor">The color of the background when the image resize does not fit the same dimensions as the original image.</param>
        /// <returns></returns>
        public static Image GetImage(FileInfo fileInfo, int width, int height, System.Drawing.Color backgroundColor)
        {
            return GetImage(GetImage(fileInfo), width, height, backgroundColor);
        }

        /// <summary>
        /// Gets a Scaled Image from an existing Image.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="size">The desired size of the returning image.</param>
        /// <returns></returns>
        public static Image GetImage(Image image, Size size)
        {
            return GetImage(image, size.Width, size.Height);
        }
        /// <summary>
        /// Gets a Scaled Image from an existing Image.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="size">The desired size of the returning image.</param>
        /// <param name="backgroundColor">The color of the background when the image resize does not fit the same dimensions as the original image.</param>
        /// <returns></returns>
        public static Image GetImage(Image image, Size size, System.Drawing.Color backgroundColor)
        {
            return GetImage(image, size.Width, size.Height, backgroundColor);
        }
        /// <summary>
        /// Gets a Scaled Image from an existing Image.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="width">The width of the image.</param>
        /// <param name="height">The height of the image.</param>
        /// <returns></returns>
        public static Image GetImage(Image image, int width, int height)
        {
            return GetImage(image, width, height, System.Drawing.Color.Transparent);
        }
        /// <summary>
        /// Gets a Scaled Image from an existing Image.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="width">The width of the image.</param>
        /// <param name="height">The height of the image.</param>
        /// <param name="backgroundColor">The color of the background when the image resize does not fit the same dimensions as the original image.</param>
        /// <returns></returns>
        public static Image GetImage(Image image, int width, int height, System.Drawing.Color backgroundColor)
        {
            if (image.Width == width && image.Height == height)
                return image;

            int sourceWidth = image.Width, sourceHeight = image.Height;
            int sourceX = 0, sourceY = 0, destX = 0, destY = 0;
            float nPercent = 0, nPercentW = 0, nPercentH = 0;

            nPercentW = ((float)width / (float)sourceWidth);
            nPercentH = ((float)height / (float)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt32((width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt32((height - (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            bmp.SetResolution(image.HorizontalResolution, image.VerticalResolution);
            using (Graphics graphics = Graphics.FromImage((Image)bmp))
            {
                graphics.Clear(backgroundColor);
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.DrawImage(image, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);
                graphics.Dispose();
            }

            return bmp;
        }

        /// <summary>
        /// Saves the Image to a specified fully qualified file name.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="fileName">The name of the image file.</param>
        /// <param name="quality">The quality of the image.</param>
        /// <returns></returns>
        public static FileInfo SaveImage(Image image, string fileName, long quality = 100L)
        {
            return SaveImage(image, new FileInfo(fileName), quality);
        }
        /// <summary>
        /// Saves the Image to a specified file.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="fileInfo">The file info to the image.</param>
        /// <param name="quality">The quality of the image.</param>
        /// <returns></returns>
        public static FileInfo SaveImage(Image image, FileInfo fileInfo, long quality = 100L)
        {
            ImageCodecInfo imageCodecInfo = null;
            if (!string.IsNullOrEmpty(fileInfo.Extension))
                imageCodecInfo = GetImageCodecInfo(fileInfo.Extension.ToLower());

            if (imageCodecInfo == null)
                imageCodecInfo = GetImageCodecInfo(ImageFormat.Png);

            if (imageCodecInfo == null)
                throw new ArgumentException(string.Format("Unsupported image format specified for encoding ({0})", fileInfo.Extension.ToLower()), "imageCodecInfo");

            return SaveImage(image, fileInfo, imageCodecInfo, quality);
        }
        /// <summary>
        /// Saves the Image to a specified fully qualified file name.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="fileName">The name of the image file.</param>
        /// <param name="imageFormat">The image format to use for saving the image.</param>
        /// <param name="quality">The quality of the image.</param>
        /// <returns></returns>
        public static FileInfo SaveImage(Image image, string fileName, ImageFormat imageFormat, long quality = 100L)
        {
            return SaveImage(image, new FileInfo(fileName), imageFormat, quality);
        }
        /// <summary>
        /// Saves the Image to a specified file.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="fileInfo">The file info to the image.</param>
        /// <param name="imageFormat">The image format to use for saving the image.</param>
        /// <param name="quality">The quality of the image.</param>
        /// <returns></returns>
        public static FileInfo SaveImage(Image image, FileInfo fileInfo, ImageFormat imageFormat, long quality = 100L)
        {
            var imageCodecInfo = GetImageCodecInfo(imageFormat);
            if (imageCodecInfo == null)
                throw new ArgumentException(string.Format("Unsupported image format specified for encoding ({0})", imageFormat), "imageFormat");

            return SaveImage(image, fileInfo, imageCodecInfo, quality);
        }
        /// <summary>
        /// Saves the Image to a specified fully qualified file name.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="fileName">The name of the image file.</param>
        /// <param name="imageCodecInfo">The image codec used to save the image.</param>
        /// <param name="quality">The quality of the image.</param>
        /// <returns></returns>
        public static FileInfo SaveImage(Image image, string fileName, ImageCodecInfo imageCodecInfo, long quality = 100L)
        {
            return SaveImage(image, new FileInfo(fileName), imageCodecInfo, quality);
        }
        /// <summary>
        /// Saves the Image to a specified file.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="fileInfo">The file info to the image.</param>
        /// <param name="imageCodecInfo">The image codec used to save the image.</param>
        /// <param name="quality">The quality of the image.</param>
        /// <returns></returns>
        public static FileInfo SaveImage(Image image, FileInfo fileInfo, ImageCodecInfo imageCodecInfo, long quality = 100L)
        {
            try
            {
                if (image != null)
                {
                    DirectoryInfo destDir = new DirectoryInfo(fileInfo.DirectoryName);

                    if (!destDir.Exists)
                        destDir.Create();

                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

                    image.Save(fileInfo.FullName, imageCodecInfo, encoderParameters);

                    return new FileInfo(fileInfo.FullName);
                }
                else
                {
                    throw new Exception("Unable to save image, the Image object was null.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to save image to '" + fileInfo.FullName + "'. " + ex.Message, ex);
            }

        }

        /// <summary>
        /// Returns the image format for the given image.
        /// </summary>
        /// <param name="image">The image to return the image format for.</param>
        /// <returns></returns>
        public static ImageFormat GetImageFormat(System.Drawing.Image image)
        {
            if (image.RawFormat.Equals(ImageFormat.Jpeg))
                return ImageFormat.Jpeg;
            if (image.RawFormat.Equals(ImageFormat.Bmp))
                return ImageFormat.Bmp;
            if (image.RawFormat.Equals(ImageFormat.Png))
                return ImageFormat.Png;
            if (image.RawFormat.Equals(ImageFormat.Emf))
                return ImageFormat.Emf;
            if (image.RawFormat.Equals(ImageFormat.Exif))
                return ImageFormat.Exif;
            if (image.RawFormat.Equals(ImageFormat.Gif))
                return ImageFormat.Gif;
            if (image.RawFormat.Equals(ImageFormat.Icon))
                return ImageFormat.Icon;
            if (image.RawFormat.Equals(ImageFormat.MemoryBmp))
                return ImageFormat.MemoryBmp;
            if (image.RawFormat.Equals(ImageFormat.Tiff))
                return ImageFormat.Tiff;
            if (image.RawFormat.Equals(ImageFormat.Wmf))
                return ImageFormat.Wmf;
            else
                throw new NotSupportedException("Unknown image format.");
        }

        /// <summary>
        /// Sets the Brightness of the image.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="brightness">A value between -256 and 256</param>
        /// <returns></returns>
        public static Image SetBrightness(Image image, int brightness)
        {
            try
            {
                if (image != null)
                {
                    if (brightness < -255 || brightness > 255)
                        return image;


                    Bitmap b = (Bitmap)(image);
                    // GDI+ still lies to us - the return format is BGR, NOT RGB.
                    BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                    int stride = bmData.Stride;
                    System.IntPtr Scan0 = bmData.Scan0;

                    int nVal = 0;
                    unsafe
                    {
                        byte* p = (byte*)(void*)Scan0;

                        int nOffset = stride - b.Width * 3;
                        int nWidth = b.Width * 3;

                        for (int y = 0; y < b.Height; ++y)
                        {
                            for (int x = 0; x < nWidth; ++x)
                            {
                                nVal = (int)(p[0] + brightness);

                                if (nVal < 0) nVal = 0;
                                if (nVal > 255) nVal = 255;

                                p[0] = (byte)nVal;

                                ++p;
                            }
                            p += nOffset;
                        }
                    }

                    b.UnlockBits(bmData);
                    image = b;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to set the Brightness of the Image. " + ex.Message);
            }

            return image;

        }
        /// <summary>
        /// Sets the Contast of the image.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="contrast">A value between -100 and 100</param>
        /// <returns></returns>
        public static Image SetContrast(Image image, int contrast)
        {
            try
            {
                if (image != null)
                {
                    if (contrast < -100 || contrast > 100)
                        return image;

                    Bitmap b = (Bitmap)(image);

                    double pixel = 0;
                    double nContrast = (100.0 + contrast) / 100.0;

                    nContrast *= nContrast;

                    int red, green, blue;

                    //GDI+ still lies to us - the return format is BGR, NOT RGB.
                    BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                    int stride = bmData.Stride;
                    System.IntPtr Scan0 = bmData.Scan0;

                    unsafe
                    {
                        byte* p = (byte*)(void*)Scan0;

                        int nOffset = stride - b.Width * 3;

                        for (int y = 0; y < b.Height; ++y)
                        {
                            for (int x = 0; x < b.Width; ++x)
                            {
                                blue = p[0];
                                green = p[1];
                                red = p[2];

                                pixel = red / 255.0;
                                pixel -= 0.5;
                                pixel *= nContrast;
                                pixel += 0.5;
                                pixel *= 255;
                                if (pixel < 0) pixel = 0;
                                if (pixel > 255) pixel = 255;
                                p[2] = (byte)pixel;

                                pixel = green / 255.0;
                                pixel -= 0.5;
                                pixel *= nContrast;
                                pixel += 0.5;
                                pixel *= 255;
                                if (pixel < 0) pixel = 0;
                                if (pixel > 255) pixel = 255;
                                p[1] = (byte)pixel;

                                pixel = blue / 255.0;
                                pixel -= 0.5;
                                pixel *= nContrast;
                                pixel += 0.5;
                                pixel *= 255;
                                if (pixel < 0) pixel = 0;
                                if (pixel > 255) pixel = 255;
                                p[0] = (byte)pixel;

                                p += 3;
                            }
                            p += nOffset;
                        }
                    }

                    b.UnlockBits(bmData);
                    image = b;
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Unable to set the Contrast of the Image. " + ex.Message);
            }

            return image;
        }
        /// <summary>
        /// Rotates an image.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static Image RotateImage(Image image, float angle)
        {
            //create a new empty bitmap to hold rotated image
            Bitmap returnBitmap = new Bitmap(image.Width, image.Height);
            //make a graphics object from the empty bitmap
            Graphics g = Graphics.FromImage(returnBitmap);
            //move rotation point to center of image
            g.TranslateTransform((float)image.Width / 2, (float)image.Height / 2);
            //rotate
            g.RotateTransform(angle);
            //move image back
            g.TranslateTransform(-(float)image.Width / 2, -(float)image.Height / 2);
            //draw passed in image onto graphics object
            g.DrawImage(image, new Point(0, 0));
            return returnBitmap;
        }
        /// <summary>
        /// Reflects an Image;
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="backgroundColor">The background color.</param>
        /// <param name="reflectivity">The level of reflectivity.</param>
        /// <returns></returns>
        public static Image ReflectImage(Image image, System.Drawing.Color backgroundColor, int reflectivity)
        {
            // Calculate the size of the new image
            int height = (int)(image.Height + (image.Height * ((float)reflectivity / 255)));
            System.Drawing.Bitmap newImage = new System.Drawing.Bitmap(image.Width, height, PixelFormat.Format24bppRgb);
            newImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                // Initialize main graphics buffer
                graphics.Clear(backgroundColor);
                graphics.DrawImage(image, new Point(0, 0));
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                Rectangle destinationRectangle = new Rectangle(0, image.Size.Height,
                                                 image.Size.Width, image.Size.Height);

                // Prepare the reflected image
                int reflectionHeight = (image.Height * reflectivity) / 255;
                System.Drawing.Image reflectedImage = new System.Drawing.Bitmap(image.Width, reflectionHeight);

                // Draw just the reflection on a second graphics buffer
                using (Graphics gReflection = Graphics.FromImage(reflectedImage))
                {
                    gReflection.DrawImage(image,
                       new Rectangle(0, 0, reflectedImage.Width, reflectedImage.Height),
                       0, image.Height - reflectedImage.Height, reflectedImage.Width,
                       reflectedImage.Height, GraphicsUnit.Pixel);
                }
                reflectedImage.RotateFlip(RotateFlipType.RotateNoneFlipY);
                Rectangle imageRectangle =
                    new Rectangle(destinationRectangle.X, destinationRectangle.Y,
                    destinationRectangle.Width,
                    (destinationRectangle.Height * reflectivity) / 255);

                // Draw the image on the original graphics
                graphics.DrawImage(reflectedImage, imageRectangle);

                // Finish the reflection using a gradiend brush
                LinearGradientBrush brush = new LinearGradientBrush(imageRectangle,
                       System.Drawing.Color.FromArgb(255 - reflectivity, backgroundColor),
                        backgroundColor, 90, false);
                graphics.FillRectangle(brush, imageRectangle);
            }

            return newImage;
        }
        /// <summary>
        /// Gets a Scaled Image from an existing Image.
        /// </summary>
        /// <param name="image">The instance of the image object.</param>
        /// <param name="width">The width of the image.</param>
        /// <param name="height">The height of the image.</param>
        /// <returns></returns>
        public static Image ScaleImage(Image image, int width, int height)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)width / (float)sourceWidth);
            nPercentH = ((float)height / (float)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((height - (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
            {
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.DrawImage(image, new Rectangle(0, 0, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);
                grPhoto.Dispose();
            }

            return bmPhoto;
        }
        #endregion
    }
}
