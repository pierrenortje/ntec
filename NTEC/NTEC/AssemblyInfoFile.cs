﻿namespace NTEC
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Represents an existing assembly file.
    /// </summary>
    public class AssemblyInfoFile
    {
        #region Private Static Fields
        private const string regexVersionFormat = @"^(?!//\s)(?:\[|<)assembly:\s{0}\(""((\d+|\*)\.(\d+|\*)\.(\d+|\*)(?:\.(\d+|\*))?)""\)(?:]|>)";
        private const string regexAttributeFormat = @"(?:\[|<)assembly:\s{0}\(""(.*)""\)(?:]|>)";

        private static Regex regexAssemblyVersion = new Regex(string.Format(regexVersionFormat, "AssemblyVersion"), RegexOptions.Multiline);
        private static Regex regexAssemblyFileVersion = new Regex(string.Format(regexVersionFormat, "AssemblyFileVersion"), RegexOptions.Multiline);
        private static Regex regexAssemblyProductName = new Regex(string.Format(regexAttributeFormat, "AssemblyProduct"), RegexOptions.Multiline);
        private static Regex regexAssemblyTitle = new Regex(string.Format(regexAttributeFormat, "AssemblyTitle"), RegexOptions.Multiline);
        #endregion

        #region Private Constructor
        private AssemblyInfoFile() { }
        #endregion

        #region Public Static Methods
        public static AssemblyInfoFile Parse(string fileName)
        {
            var assemblyInfoFile = new AssemblyInfoFile
            {
                FileName = fileName
            };

            assemblyInfoFile.Load();

            return assemblyInfoFile;
        }
        #endregion

        #region Public Properties
        public string ProductName { get; set; }
        public string FileName { get; set; }
        public System.Version Version { get; set; }
        public System.Version FileVersion { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the current instance.
        /// </summary>
        public void Load()
        {
            string fileContent = File.ReadAllText(FileName);

            var matchAssemblyProductName = regexAssemblyProductName.Match(fileContent);
            if (matchAssemblyProductName.Success)
                this.ProductName = matchAssemblyProductName.Groups[1].Value;
            else
            {
                var matchAssemblyTitle = regexAssemblyTitle.Match(fileContent);
                if (matchAssemblyTitle.Success)
                    this.ProductName = matchAssemblyTitle.Groups[1].Value;
                else
                    throw new Exception($"Missing product and title information in '{FileName}'");
            }

            var matchAssemblyVersion = regexAssemblyVersion.Match(fileContent);
            if (matchAssemblyVersion.Success)
                this.Version = new System.Version(matchAssemblyVersion.Groups[1].Value);
            else
                throw new Exception($"Missing version information in '{FileName}'");

            var fileVersionCurrent = new Version();
            var matchAssemblyFileVersion = regexAssemblyFileVersion.Match(fileContent);
            if (matchAssemblyFileVersion.Success)
                this.FileVersion = new System.Version(matchAssemblyFileVersion.Groups[1].Value);
        }
        /// <summary>
        /// Updates the assembly file with the specified version.
        /// </summary>
        /// <param name="version">The version to update to.</param>
        /// <returns>The file contents of the updated assembly file.</returns>
        public string Update(System.Version version)
        {
            string fileContent = File.ReadAllText(FileName);

            var matchAssemblyVersion = regexAssemblyVersion.Match(fileContent);
            if (matchAssemblyVersion.Success)
                fileContent = fileContent.Replace(matchAssemblyVersion.Value, Regex.Replace(matchAssemblyVersion.Value, @"((\d+|\*)\.(\d+|\*)\.(\d+|\*)\.(\d+|\*))", version.ToString()));

            var matchAssemblyFileVersion = regexAssemblyFileVersion.Match(fileContent);
            if (matchAssemblyFileVersion.Success)
                fileContent = fileContent.Replace(matchAssemblyFileVersion.Value, Regex.Replace(matchAssemblyFileVersion.Value, @"((\d+|\*)\.(\d+|\*)\.(\d+|\*)\.(\d+|\*))", version.ToString()));

            return fileContent;
        }
        #endregion
    }

    /// <summary>
    /// A collection of assembly files.
    /// </summary>
    public class AssemblyInfoFiles : List<AssemblyInfoFile>
    {
        /// <summary>
        /// Get the maximum version inside a collection of assemblies.
        /// </summary>
        public System.Version MaxVersion
        {
            get
            {
                var maxVersion = new System.Version();
                foreach (AssemblyInfoFile assemblyInfoFile in this)
                {
                    if (assemblyInfoFile.Version > maxVersion)
                        maxVersion = assemblyInfoFile.Version;

                    if (assemblyInfoFile.FileVersion > maxVersion)
                        maxVersion = assemblyInfoFile.FileVersion;
                }
                return maxVersion;
            }
        }
    }
}