﻿namespace NTEC
{
    using System;

    public static class DateTime
    {
        /// <summary>
        /// Creates a shortend, readable string for dates.
        /// </summary>
        /// <param name="value">The System.DateTime value to parse.</param>
        /// <returns>A string of the specified System.DateTime value.</returns>
        public static string ToReadableTime(this System.DateTime value)
        {
            var timeSpan = new TimeSpan(System.DateTime.UtcNow.Ticks - value.Ticks);
            double delta = timeSpan.TotalSeconds;
            if (delta < 60)
            {
                return timeSpan.Seconds == 1 ? "one second ago" : timeSpan.Seconds + " seconds ago";
            }
            if (delta < 120)
            {
                return "a minute ago";
            }
            if (delta < 2700)
            {
                return timeSpan.Minutes + " minutes ago";
            }
            if (delta < 5400)
            {
                return "an hour ago";
            }
            if (delta < 86400)
            {
                return timeSpan.Hours + " hours ago";
            }
            if (delta < 172800)
            {
                return "yesterday";
            }
            if (delta < 2592000)
            {
                return timeSpan.Days + " days ago";
            }
            if (delta < 31104000)
            {
                int months = Convert.ToInt32(Math.Floor((double)timeSpan.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            var years = Convert.ToInt32(Math.Floor((double)timeSpan.Days / 365));
            return years <= 1 ? "one year ago" : years + " years ago";
        }
    }
}
