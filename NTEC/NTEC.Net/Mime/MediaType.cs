﻿namespace NTEC.Net.Mime
{
    public static class MediaType
    {
        /// <summary>
        /// The MIME type that respresents JSON content.
        /// </summary>
        public const string JSON = "application/json";
    }
}