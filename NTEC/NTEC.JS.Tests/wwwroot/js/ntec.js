var ntec = {
    crypto: {
        sym: {
            aesGcm: {}
        },
        asym: {
            rsa: {}
        }
    }
};

ntec.crypto.sym.aesGcm = {

    ///
    /// A function to encrypt AES-GCM
    ///
    encrypt: async function (plaintext, base64Key) {
        const key = await this.importKey(base64Key);
        const encoder = new TextEncoder();

        const nonce = crypto.getRandomValues(new Uint8Array(12)); // 96-bit nonce
        const plaintextBytes = encoder.encode(plaintext);

        const ciphertext = await crypto.subtle.encrypt(
            {
                name: "AES-GCM",
                iv: nonce,
            },
            key,
            plaintextBytes
        );

        return {
            ciphertext: btoa(String.fromCharCode(...new Uint8Array(ciphertext))), // Base64 encode ciphertext
            nonce: btoa(String.fromCharCode(...nonce)), // Base64 encode nonce
        };
    },

    ///
    /// A function to decrypt AES-GCM
    ///
    decrypt: async function (base64Ciphertext, base64Nonce, key, base64Tag) {
        const decoder = new TextDecoder();

        let ciphertext = Uint8Array.from(atob(base64Ciphertext), c => c.charCodeAt(0));
        if (base64Tag) {
            const tag = Uint8Array.from(atob(base64Tag), c => c.charCodeAt(0));
            ciphertext = new Uint8Array([...ciphertext, ...tag]);
        }

        const nonce = Uint8Array.from(atob(base64Nonce), c => c.charCodeAt(0));

        const plaintextBytes = await crypto.subtle.decrypt(
            {
                name: "AES-GCM",
                iv: nonce,
            },
            key,
            ciphertext
        );

        return decoder.decode(plaintextBytes);
    },

    importKey: function (base64Key) {
        const rawKey = Uint8Array.from(atob(base64Key), c => c.charCodeAt(0));
        return crypto.subtle.importKey(
            "raw",
            rawKey,
            {
                name: "AES-GCM",
            },
            true,
            ["encrypt", "decrypt"]
        );
    },

    generatePrivateKey: async function () {
        try {
            // Generate an AES-GCM key (256 bits)
            const key = await crypto.subtle.generateKey(
                {
                    name: "AES-GCM",
                    length: 256, // AES-256 key
                },
                true,
                ["encrypt", "decrypt"] // Usages: key can be used for encryption and decryption
            );

            return key;
        } catch (err) {
            console.error("Error generating AES-GCM key:", err);
        }
    }
}

ntec.crypto.asym.rsa = {
    generateKeyPair: async function () {
        const keyPair = await window.crypto.subtle.generateKey(
            {
                name: "RSA-OAEP",
                modulusLength: 2048, // Key size
                publicExponent: new Uint8Array([1, 0, 1]), // 65537
                hash: { name: "SHA-256" }, // Hash function
            },
            true, // Extractable (allows exporting the keys)
            ["encrypt", "decrypt"] // Key usage
        );
        return keyPair;
    },

    // Encrypt data with the public key
    encryptData: async function (publicKey, data) {
        var spkiBuffer = this.xmlToSpki(publicKey);
        const key = await crypto.subtle.importKey(
            "spki",
            spkiBuffer,
            {
                name: "RSA-OAEP",
                hash: { name: "SHA-256" },
            },
            true,                   // Whether the key is extractable
            ["encrypt"]
        );

        const encodedData = new TextEncoder().encode(data); // Convert to Uint8Array
        const encryptedData = await window.crypto.subtle.encrypt(
            {
                name: "RSA-OAEP",
            },
            key,
            encodedData
        );
        return encryptedData;
    },

    encryptDataV2: async function (cryptoKey, data) {
        const encodedData = new TextEncoder().encode(data);

        // Encrypt the data
        const encryptedData = await crypto.subtle.encrypt(
            {
                name: "RSA-OAEP",
            },
            cryptoKey,
            encodedData
        );

        return btoa(String.fromCharCode(...new Uint8Array(encryptedData)));
    },

    // Decrypt data with the private key
    decryptData: async function (privateKey, encryptedData) {
        const decryptedData = await window.crypto.subtle.decrypt(
            {
                name: "RSA-OAEP",
            },
            privateKey,
            encryptedData
        );
        return new TextDecoder().decode(decryptedData); // Convert back to string
    },

    // Export a public or private key to JWK format (JSON Web Key)
    exportKey: async function (key) {
        const exportedKey = await window.crypto.subtle.exportKey("jwk", key);
        return exportedKey;
    },

    base64UrlToUint8Array: function (base64Url) {
        const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
        const binaryString = atob(base64);
        const len = binaryString.length;
        const bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
            bytes[i] = binaryString.charCodeAt(i);
        }
        return bytes;
    },

    buildSpki: function (modulus, exponent) {
        const modulusLength = modulus.length;
        const exponentLength = exponent.length;

        const modulusHeader = modulusLength > 127
            ? [0x82, modulusLength >> 8, modulusLength & 0xff]
            : [0x81, modulusLength];

        const exponentHeader = [exponentLength];

        const derSequence = [
            0x30, // SEQUENCE
            0x82, // Length
            ((modulusLength + modulusHeader.length + 2 + exponentLength + exponentHeader.length + 2) >> 8) & 0xff,
            (modulusLength + modulusHeader.length + 2 + exponentLength + exponentHeader.length + 2) & 0xff,
            0x02, // INTEGER (modulus)
            ...modulusHeader,
            ...modulus,
            0x02, // INTEGER (exponent)
            ...exponentHeader,
            ...exponent,
        ];

        const spkiHeader = [
            0x30, 0x82, // SEQUENCE
            ((derSequence.length + 15) >> 8) & 0xff,
            (derSequence.length + 15) & 0xff,
            0x30, 0x0d, // AlgorithmIdentifier
            0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01, // OID (RSA Encryption)
            0x05, 0x00, // NULL
            0x03, 0x82, // BIT STRING
            ((derSequence.length + 1) >> 8) & 0xff,
            (derSequence.length + 1) & 0xff,
            0x00, // Unused bits
            ...derSequence,
        ];

        return new Uint8Array(spkiHeader).buffer;
    }
}