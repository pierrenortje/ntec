﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace NTEC.NET6.Cryptography.Test
{
    [TestClass]
    public class JWETests
    {
        [TestMethod]
        public void TestJWK()
        {
            var jwks = new JsonWebKeySet();  // Generate an RSA key and add to the JsonWebKeySet

            using var rsa = System.Security.Cryptography.RSA.Create(2048);
            var rsaParameters = rsa.ExportParameters(false);
            var rsaKey = new JsonWebKey
            {
                Kty = "RSA", // Key Type
                Use = "sig", // Key Usage: "sig" for signing
                Kid = Guid.NewGuid().ToString(), // Key Identifier
                Alg = "RS256", // Algorithm
                N = Base64UrlEncoder.Encode(rsaParameters.Modulus), // Modulus
                E = Base64UrlEncoder.Encode(rsaParameters.Exponent) // Exponent
            };
            jwks.Keys.Add(rsaKey);

            // Generate an EC key and add to the JsonWebKeySet
            using var ecdsa = ECDsa.Create(ECCurve.NamedCurves.nistP256);
            var ecParameters = ecdsa.ExportParameters(false);
            var ecKey = new JsonWebKey
            {
                Kty = "EC", // Key Type
                Use = "sig", // Key Usage: "sig" for signing
                Kid = Guid.NewGuid().ToString(), // Key Identifier
                Alg = "ES256", // Algorithm
                Crv = "P-256", // Curve
                X = Base64UrlEncoder.Encode(ecParameters.Q.X), // X-coordinate
                Y = Base64UrlEncoder.Encode(ecParameters.Q.Y) // Y-coordinate
            };
            jwks.Keys.Add(ecKey);

            // Serialize the JsonWebKeySet to JSON
            string jwksJson = JsonConvert.SerializeObject(jwks, Formatting.Indented, new JsonSerializerSettings{
                NullValueHandling = NullValueHandling.Ignore
            });
            Console.WriteLine("JSON Web Key Set:");
            Console.WriteLine(jwksJson);
        }

        [TestMethod]
        public void TestJWE_NoCert()
        {
            //using var rsa = RSA.Create(2048);
            //var publicKey = new RsaSecurityKey(rsa.ExportParameters(false));
            //var privateKey = new RsaSecurityKey(rsa.ExportParameters(true));

            //RunEncryptionTest(publicKey, privateKey);
        }

        [TestMethod]
        public void TestJWE_WithCert()
        {
            var publicCert = new X509Certificate2(@"C:\path\to\domain.crt");
            var privateCert = new X509Certificate2(@"C:\path\to\domain.pfx", "password");

            // Create a SecurityKey from the certificate
            var publicKey = new X509SecurityKey(publicCert);
            var privateKey = new X509SecurityKey(privateCert);

            RunEncryptionTest(publicKey, privateKey);
        }

        private void RunEncryptionTest(SecurityKey publicKey, SecurityKey privateKey)
        {
            var signCredentials = new SigningCredentials(privateKey, SecurityAlgorithms.RsaSha256);

            var enc = JWE.Encrypt(new List<Claim>{
                new Claim("name","John")
            }, publicKey, signCredentials, "me", "you");

            var dec = JWE.Decrypt(enc, publicKey, privateKey);

            Assert.IsTrue(dec != null);
        }
    }
}