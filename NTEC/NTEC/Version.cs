﻿namespace NTEC
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    #region Public Enums
    public enum VersionComponentUpdateType
    {
        Ignore,
        Increment,
        Year,
        /// <summary>
        /// Month Day (Mdd)
        /// </summary>
        MonthDay,
        /// <summary>
        /// MS version date format (yyDoY)
        /// </summary>
        DateFormat_MS
    }
    #endregion

    #region Public Classes
    public class Version : EventOccuredBase
    {
        #region Private Methods
        private System.Version GetUpdatedVersion(System.Version version, VersionComponentUpdateType major, VersionComponentUpdateType minor, VersionComponentUpdateType build, VersionComponentUpdateType revision)
        {
            return new System.Version(GetVersionComponentValue(major, version.Major), GetVersionComponentValue(minor, version.Minor), GetVersionComponentValue(build, version.Build), GetVersionComponentValue(revision, version.Revision));
        }
        private System.Version GetUpdatedVersion(System.Version version, VersionComponentUpdateType major, VersionComponentUpdateType minor, VersionComponentUpdateType build)
        {
            return new System.Version(GetVersionComponentValue(major, version.Major), GetVersionComponentValue(minor, version.Minor), GetVersionComponentValue(build, version.Build));
        }

        private int GetVersionComponentValue(VersionComponentUpdateType versionComponentUpdateType, int versionComponentValue)
        {
            switch (versionComponentUpdateType)
            {
                case VersionComponentUpdateType.Increment:
                    return versionComponentValue + 1;

                case VersionComponentUpdateType.Year:
                    return System.DateTime.Now.Year;

                case VersionComponentUpdateType.MonthDay:
                    return int.Parse($"{System.DateTime.Today.ToString("Mdd")}");

                case VersionComponentUpdateType.DateFormat_MS:
                    return int.Parse($"{System.DateTime.Today.ToString("yy")}{System.DateTime.Today.DayOfYear}");
            }

            return versionComponentValue;
        }
        #endregion

        #region Public Async Methods
        /// <summary>
        /// Retrieve all assembly files within the specified solution.
        /// </summary>
        /// <param name="solutionPath">The path to the solution to look for assemblies.</param>
        /// <param name="excludedPaths">Paths to exclude when searching for assemblies.</param>
        /// <returns>A list of assembly files.</returns>
        public async Task<AssemblyInfoFiles> GetAssemblyInfoFilesAsync(string solutionPath, params string[] excludedPaths)
        {
            OnEventOccured("Retrieving assembly info files...");

            var assemblyInfoFiles = new AssemblyInfoFiles();
            await Task.Run(() =>
            {
                var files = Directory.GetFiles(Path.GetDirectoryName(solutionPath), "AssemblyInfo.cs", SearchOption.AllDirectories);
                foreach (string file in files)
                {
                    if (!excludedPaths.Any(e => file.ToLower().Contains(e.ToLower())))
                        assemblyInfoFiles.Add(AssemblyInfoFile.Parse(file));
                }
            });

            return assemblyInfoFiles;
        }

        /// <summary>
        /// Update all assemblies to the specified version.
        /// </summary>
        /// <param name="assemblyInfoFiles">The assembly files to update.</param>
        /// <param name="major">The value of the major version.</param>
        /// <param name="minor">The value of the minor version.</param>
        /// <param name="build">The value of the build version.</param>
        /// <param name="revision">The value of the revision version.</param>
        /// <returns>The task.</returns>
        public async Task UpdateAsync(AssemblyInfoFiles assemblyInfoFiles, VersionComponentUpdateType major, VersionComponentUpdateType minor, VersionComponentUpdateType build, VersionComponentUpdateType revision)
        {
            OnEventOccured("Assembly info files versioning started...");

            if (assemblyInfoFiles.Count == 0)
            {
                OnEventOccured("No files found to version.", EventType.Warning);
                return;
            }

            await Task.Run(() =>
            {
                foreach (AssemblyInfoFile assemblyInfoFile in assemblyInfoFiles)
                {
                    assemblyInfoFile.Load();
                }

                int maxProductNameLength = assemblyInfoFiles.Max(t => t.ProductName.Length);
                var newVersion = GetUpdatedVersion(assemblyInfoFiles.MaxVersion, major, minor, build, revision);
                foreach (AssemblyInfoFile assemblyInfoFile in assemblyInfoFiles)
                {
                    OnEventOccured($"{assemblyInfoFile.ProductName.PadRight(maxProductNameLength)}\t{assemblyInfoFile.Version} > {newVersion}");

                    string contents = File.ReadAllText(assemblyInfoFile.FileName, Encoding.UTF8);

                    contents = assemblyInfoFile.Update(newVersion);

                    File.WriteAllText(assemblyInfoFile.FileName, contents, Encoding.UTF8);
                }
            });

            OnEventOccured("Assembly info files versioning completed.");
        }
        #endregion
    }
    #endregion
}