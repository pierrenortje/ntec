﻿namespace NTEC.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public abstract class TestBase
    {
        protected const string PASSWORD = "password";
        protected const string TEMP_PATH = @"C:\Temp\";
        protected const string XML_FILE_PATH = TEMP_PATH + "MainBanner.png";
        protected const string SHARE_IPADDRESS = "192.168.8.101";
    }
}