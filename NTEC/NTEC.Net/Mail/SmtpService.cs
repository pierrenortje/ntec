﻿namespace NTEC.Net.Mail
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Allows applications to send e-mail by using the Simple Mail Transfer Protocol (SMTP).
    /// </summary>
    public class SmtpService : IDisposable
    {
        #region Private Fields
        private readonly string host;
        private readonly int port;

        private SmtpClient client = null;
        private MailMessage message = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NTEC.Net.Mail class.
        /// </summary>
        public SmtpService()
        {
            if (!string.IsNullOrEmpty(this.host))
                if (this.port > 0)
                    this.client = new SmtpClient(this.host, this.port);
                else
                    this.client = new SmtpClient(this.host);
            else
                this.client = new SmtpClient();

            this.message = new MailMessage();
        }
        /// <summary>
        /// Initializes a new instance of the NTEC.Net.Mail class.
        /// </summary>
        /// <param name="host">A System.String that contains the name or IP address of the host used for SMTP transactions.</param>
        public SmtpService(string host)
                : this()
        {
            this.host = host;
        }
        /// <summary>
        /// Initializes a new instance of the NTEC.Net.Mail class.
        /// </summary>
        /// <param name="host">A System.String that contains the name or IP address of the host used for SMTP transactions.</param>
        /// <param name="port">An System.Int32 greater than zero that contains the port to be used on host.</param>
        public SmtpService(string host, int port)
                : this(host)
        {
            this.port = port;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the credentials used to authenticate the sender.
        /// </summary>
        public ICredentialsByHost Credentials
        {
            set
            {
                this.client.Credentials = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="recipientAddress">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public void Send(string recipientAddress, string subject, string body)
        {
            this.Send(null, recipientAddress, subject, body);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="senderAddress">The address of the message sender.</param>
        /// <param name="recipientAddress">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public void Send(string senderAddress, string recipientAddress, string subject, string body)
        {
            MailAddress sender = null;
            if (!string.IsNullOrEmpty(senderAddress))
                sender = new MailAddress(senderAddress);

            var recipient = new MailAddress(recipientAddress);
            this.Send(sender, recipient, subject, body);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="recipient">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public void Send(MailAddress recipient, string subject, string body)
        {
            this.Send(null, recipient, subject, body);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipient">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public void Send(MailAddress sender, MailAddress recipient, string subject, string body)
        {
            var recipients = new MailAddressCollection();
            recipients.Add(recipient);
            this.Send(sender, recipients, subject, body);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public void Send(MailAddressCollection recipients, string subject, string body)
        {
            this.Send(null, recipients, subject, body, MailPriority.Normal);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public void Send(MailAddress sender, MailAddressCollection recipients, string subject, string body)
        {
            this.Send(sender, recipients, subject, body, MailPriority.Normal);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="recipient">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        public void Send(MailAddress recipient, string subject, string body, MailPriority priority)
        {
            var recipients = new MailAddressCollection();
            recipients.Add(recipient);
            this.Send(null, recipients, subject, body, priority);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipient">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        public void Send(MailAddress sender, MailAddress recipient, string subject, string body, MailPriority priority)
        {
            var recipients = new MailAddressCollection();
            recipients.Add(recipient);
            this.Send(sender, recipients, subject, body, priority);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        public void Send(MailAddressCollection recipients, string subject, string body, MailPriority priority)
        {
            this.Send(null, recipients, subject, body, priority);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        public void Send(MailAddress sender, MailAddressCollection recipients, string subject, string body, MailPriority priority)
        {
            this.Send(sender, recipients, subject, body, priority, null);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        /// <param name="resourceList">A list of resources to embed into the e-mail.</param>
        public void Send(MailAddressCollection recipients, string subject, string body, MailPriority priority, Dictionary<string, Guid> resourceList)
        {
            this.Send(null, recipients, subject, body, priority, resourceList);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        /// <param name="resourceList">A list of resources to embed into the e-mail.</param>
        public void Send(MailAddress sender, MailAddressCollection recipients, string subject, string body, MailPriority priority, Dictionary<string, Guid> resourceList)
        {
            if (sender != null)
                this.message.From = sender;

            foreach (var recipient in recipients)
                this.message.To.Add(recipient);

            this.message.Subject = subject;
            this.message.Body = body;
            this.message.IsBodyHtml = true;
            this.message.Priority = priority;

            if (resourceList != null)
            {
                var view = AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html");
                foreach (var resource in resourceList)
                {
                    var linkedResource = new LinkedResource(resource.Key);
                    linkedResource.ContentId = resource.Value.ToString();
                    view.LinkedResources.Add(linkedResource);
                }

                this.message.AlternateViews.Add(view);
            }

            this.client.Send(this.message);
        }

        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="recipientAddress">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public async Task SendAsync(string recipientAddress, string subject, string body)
        {
            await this.SendAsync(null, recipientAddress, subject, body);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="senderAddress">The address of the message sender.</param>
        /// <param name="recipientAddress">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public async Task SendAsync(string senderAddress, string recipientAddress, string subject, string body)
        {
            var sender = new MailAddress(senderAddress);
            var recipient = new MailAddress(recipientAddress);
            await this.SendAsync(sender, recipient, subject, body);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="recipient">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public async Task SendAsync(MailAddress recipient, string subject, string body)
        {
            await this.SendAsync(null, recipient, subject, body);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipient">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public async Task SendAsync(MailAddress sender, MailAddress recipient, string subject, string body)
        {
            var recipients = new MailAddressCollection();
            recipients.Add(recipient);
            await this.SendAsync(sender, recipients, subject, body);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public async Task SendAsync(MailAddressCollection recipients, string subject, string body)
        {
            await this.SendAsync(null, recipients, subject, body, MailPriority.Normal);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        public async Task SendAsync(MailAddress sender, MailAddressCollection recipients, string subject, string body)
        {
            await this.SendAsync(sender, recipients, subject, body, MailPriority.Normal);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="recipient">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        public async Task SendAsync(MailAddress recipient, string subject, string body, MailPriority priority)
        {
            var recipients = new MailAddressCollection();
            recipients.Add(recipient);
            await this.SendAsync(null, recipients, subject, body, priority);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipient">The address that the message is sent to.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        public async Task SendAsync(MailAddress sender, MailAddress recipient, string subject, string body, MailPriority priority)
        {
            var recipients = new MailAddressCollection();
            recipients.Add(recipient);
            await this.SendAsync(sender, recipients, subject, body, priority);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        public async Task SendAsync(MailAddressCollection recipients, string subject, string body, MailPriority priority)
        {
            await this.SendAsync(null, recipients, subject, body, priority);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        public async Task SendAsync(MailAddress sender, MailAddressCollection recipients, string subject, string body, MailPriority priority)
        {
            await this.SendAsync(sender, recipients, subject, body, priority, null);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        /// <param name="resourceList">A list of resources to embed into the e-mail.</param>
        public async Task SendAsync(MailAddressCollection recipients, string subject, string body, MailPriority priority, Dictionary<string, Guid> resourceList)
        {
            await this.SendAsync(null, recipients, subject, body, priority, resourceList);
        }
        /// <summary>
        /// Sends the specified message to an SMTP server for delivery as an asynchronous operation.
        /// </summary>
        /// <param name="sender">The address information of the message sender.</param>
        /// <param name="recipients">A list of recipients.</param>
        /// <param name="subject">The subject line for the message.</param>
        /// <param name="body">The message body.</param>
        /// <param name="priority">The message priority.</param>
        /// <param name="resourceList">A list of resources to embed into the e-mail.</param>
        public async Task SendAsync(MailAddress sender, MailAddressCollection recipients, string subject, string body, MailPriority priority, Dictionary<string, Guid> resourceList)
        {
            if (sender != null)
                this.message.From = sender;

            foreach (var recipient in recipients)
                this.message.To.Add(recipient);

            this.message.Subject = subject;
            this.message.Body = body;
            this.message.IsBodyHtml = true;
            this.message.Priority = priority;

            if (resourceList != null)
            {
                var view = AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html");
                foreach (var resource in resourceList)
                {
                    var linkedResource = new LinkedResource(resource.Key);
                    linkedResource.ContentId = resource.Value.ToString();
                    view.LinkedResources.Add(linkedResource);
                }

                this.message.AlternateViews.Add(view);
            }

            await this.client.SendMailAsync(this.message);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            if (this.message != null)
            {
                this.message.Dispose();
                this.message = null;
            }

            if (this.client != null)
            {
                this.client.Dispose();
                this.client = null;
            }
        }
        #endregion
    }
}
