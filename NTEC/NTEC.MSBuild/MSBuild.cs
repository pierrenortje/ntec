﻿namespace NTEC.MSBuild
{
    using NTEC.Diagnostics;
    using System;
    using System.Threading.Tasks;

    public class MSBuild : ProcessControl
    {
        #region Private Static Fields
        private const string visualStudioVersionMajorMinor = "14.0";

        private static string fileName = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)}\MSBuild\{visualStudioVersionMajorMinor}\bin\msbuild.exe";
        private static new string[] warningMessages = new string[] {
            "Build warning ",
            ": warning MSB",
            ": warning CS"
        };
        private static new string[] errorMessages = new string[] {
            "Build warning ",
            ": warning MSB",
            ": warning CS"
        };
        #endregion

        #region Constructor
        public MSBuild() : base(fileName, warningMessages, errorMessages) { }
        #endregion

        #region Public Async Methods
        /// <summary>
        /// Cleans the specified solution.
        /// </summary>
        /// <param name="solutionFilePath">The path of the solution to clean.</param>
        /// <param name="configuration">The name of the configuration to clean.</param>
        /// <returns>A task.</returns>
        public async Task CleanAsync(string solutionFilePath, string configuration = "Release")
        {
            OnEventOccured("Clean started...");

            await ExecuteAsync
            (
                $@"/nologo",
                $@"/t:Clean",
                $@"/p:Configuration={configuration}",
                $@"/tv:{visualStudioVersionMajorMinor}",
                $@"/p:VisualStudioVersion={visualStudioVersionMajorMinor}",
                $@"/maxcpucount:{Environment.ProcessorCount}",
                $@"/v:m",
                $@"/ds",
                $@"""{solutionFilePath}"""
            );

            OnEventOccured("Clean completed.");
        }
        /// <summary>
        /// Builds the specified solution.
        /// </summary>
        /// <param name="solutionFilePath">The path of the solution to build.</param>
        /// <param name="configuration">The name of the configuration to build.</param>
        /// <param name="ignoreProjectExtensions">The project extensions to ignore when building.</param>
        /// <param name="logFileName"></param>
        /// <returns>A task.</returns>
        public async Task BuildAsync(string solutionFilePath, string configuration = "Release", string ignoreProjectExtensions = null)
        {
            OnEventOccured("Build started...");

            await ExecuteAsync
            (
                $@"/nologo",
                $@"/t:Rebuild",
                $@"/p:Configuration={configuration}",
                $@"/p:WarningLevel=1",
                $@"/tv:{visualStudioVersionMajorMinor}",
                $@"/p:VisualStudioVersion={visualStudioVersionMajorMinor}",
                $@"/maxcpucount:{Environment.ProcessorCount}",
                $@"/v:m",
                $@"/ds",
                ignoreProjectExtensions != null ? $@"/ignore:{ignoreProjectExtensions}" : null,
                $@"""{solutionFilePath}"""
            );

            OnEventOccured("Build completed.");
        }
        /// <summary>
        /// Publishes the solution.
        /// </summary>
        /// <param name="solutionFilePath">The path of the solution to use for publishing.</param>
        /// <param name="configuration">The name of the configuration to use for publishing.</param>
        /// <param name="publishProfileName">The name of the publishing profile.</param>
        /// <returns>A task.</returns>
        public async Task PublishAsync(string solutionFilePath, string configuration = "Release", string publishProfileName = null)
        {
            OnEventOccured("Publish started...");

            await ExecuteAsync
            (
                $@"/nologo",
                $@"/ds",
                $@"/tv:{visualStudioVersionMajorMinor}",
                $@"/p:VisualStudioVersion={visualStudioVersionMajorMinor}",
                $@"/t:Rebuild",
                $@"/v:m",
                $@"/p:WarningLevel=1",
                $@"/maxcpucount:{Environment.ProcessorCount}",
                $@"/p:Configuration={configuration}",
                $@"/p:DeployOnBuild=true /p:PublishProfile=""{publishProfileName}""",
                $@"""{solutionFilePath}"""
            );

            OnEventOccured("Publish completed.");
        }
        #endregion
    }
}