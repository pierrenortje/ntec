﻿namespace NTEC.Security
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    public class Cryptography
    {
        #region Private Static Fields
        private const char DELIMITER = ':';

        private const int PBKDF2_ITERATIONS = 4096;

        private const int SALT_BYTE_SIZE = 24;
        private const int HASH_BYTE_SIZE = 20;

        private const int ITERATION_INDEX = 0;
        private const int SALT_INDEX = 1;
        private const int PBKDF2_INDEX = 2;

        private const int KEY_SIZE = 256;

        private const string HASH_NAME = "SHA1";
        #endregion

        #region Private Fields
        private byte[] iv;
        #endregion

        #region Public Properties
        public byte[] IV
        {
            get
            {
                return this.iv;
            }
        }
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Get the hashed value of a password.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <returns>The hashed value of the password.</returns>
        public static string HashPassword(string password)
        {
            byte[] salt = new byte[SALT_BYTE_SIZE];
            using (var cryptoProvider = new RNGCryptoServiceProvider())
            {
                cryptoProvider.GetBytes(salt);
            }

            var hash = GetPbkdf2Bytes(password, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE);
            return string.Concat(PBKDF2_ITERATIONS, DELIMITER, Convert.ToBase64String(salt), DELIMITER, Convert.ToBase64String(hash));
        }
        /// <summary>
        /// Validates a password with a hash value.
        /// </summary>
        /// <param name="password">The password to verify.</param>
        /// <param name="hashedValue">The hashed value to compare.</param>
        /// <returns>The result of the comparison.</returns>
        public static bool ValidatePassword(string password, string hashedValue)
        {
            var split = hashedValue.Split(DELIMITER);
            var iterations = int.Parse(split[ITERATION_INDEX]);
            var salt = Convert.FromBase64String(split[SALT_INDEX]);
            var hash = Convert.FromBase64String(split[PBKDF2_INDEX]);

            var hashResult = GetPbkdf2Bytes(password, salt, iterations, hash.Length);
            return SlowEquals(hash, hashResult);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Encrypts a message.
        /// </summary>
        /// <param name="secretMessage">The message to encrypt.</param>
        /// <param name="password">The password to be used for encrypting.</param>
        /// <returns>An encrypted byte array.</returns>
        public string Encrypt(string secretMessage, string password)
        {
            return Encrypt<AesManaged>(secretMessage, password);
        }
        /// <summary>
        /// Encrypts a message.
        /// </summary>
        /// <typeparam name="T">SymmetricAlgorithm</typeparam>
        /// <param name="secretMessage">The message to encrypt.</param>
        /// <param name="password">The password to be used for encrypting.</param>
        /// <returns>An encrypted byte array.</returns>
        public string Encrypt<T>(string secretMessage, string password) where T : SymmetricAlgorithm, new()
        {
            byte[] encryptedMessage;
            byte[] salt = new byte[SALT_BYTE_SIZE];

            using (var symmetricAlgorithm = new T())
            {
                using (var cryptoProvider = new RNGCryptoServiceProvider())
                {
                    cryptoProvider.GetBytes(salt);
                }

                var passwordBytes = new PasswordDeriveBytes(password, salt, HASH_NAME, PBKDF2_ITERATIONS);
                byte[] keyBytes = passwordBytes.GetBytes(KEY_SIZE / 8);

                symmetricAlgorithm.Mode = CipherMode.CBC;
                this.iv = symmetricAlgorithm.IV;

                using (var ciphertext = new MemoryStream())
                {
                    using (ICryptoTransform encryptor = symmetricAlgorithm.CreateEncryptor(keyBytes, this.iv))
                    {
                        using (var cryptoStream = new CryptoStream(ciphertext, encryptor, CryptoStreamMode.Write))
                        {
                            byte[] ciphertextMessage = Encoding.UTF8.GetBytes(secretMessage);
                            cryptoStream.Write(ciphertextMessage, 0, ciphertextMessage.Length);
                        }
                    }

                    encryptedMessage = ciphertext.ToArray();
                }
            }

            return string.Concat(PBKDF2_ITERATIONS, DELIMITER, Convert.ToBase64String(salt), DELIMITER, Convert.ToBase64String(encryptedMessage));
        }

        /// <summary>
        /// Encrypts a file's contents.
        /// </summary>
        /// <param name="filePath">The path to the file.</param>
        /// <param name="password">The password to be used for encrypting.</param>
        /// <returns>An encrypted byte array.</returns>
        public void EncryptFile(string filePath, string password)
        {
            EncryptFile<AesManaged>(filePath, password);
        }
        /// <summary>
        /// Encrypts a file's contents.
        /// </summary>
        /// <typeparam name="T">SymmetricAlgorithm</typeparam>
        /// <param name="filePath">The path to the file.</param>
        /// <param name="password">The password to be used for encrypting.</param>
        /// <returns>An encrypted byte array.</returns>
        public void EncryptFile<T>(string filePath, string password) where T : SymmetricAlgorithm, new()
        {
            if (!File.Exists(filePath))
                throw new FileNotFoundException();

            string fileContent = File.ReadAllText(filePath);
            string encryptedContent = Encrypt<T>(fileContent, password);

            using (var streamWriter = new StreamWriter(filePath, append: false))
            {
                streamWriter.Write(encryptedContent);
            }
        }

        /// <summary>
        /// Decrypts an encrypted message.
        /// </summary>
        /// <param name="secretMessage">The message to decrypt.</param>
        /// <param name="password">The password to be used for decrypting.</param>
        /// <param name="iv">The initialization vector to use.</param>
        /// <returns>A decrypted message.</returns>
        public string Decrypt(string secretMessage, string password, byte[] iv)
        {
            return Decrypt<AesManaged>(secretMessage, password, iv);
        }
        /// <summary>
        /// Decrypts an encrypted message.
        /// </summary>
        /// <typeparam name="T">SymmetricAlgorithm</typeparam>
        /// <param name="secretMessage">The message to decrypt.</param>
        /// <param name="password">The password to be used for decrypting.</param>
        /// <param name="iv">The initialization vector to use.</param>
        /// <returns>A decrypted message.</returns>
        public string Decrypt<T>(string secretMessage, string password, byte[] iv) where T : SymmetricAlgorithm, new()
        {
            string decryptedMessage;

            using (var symmetricAlgorithm = new T())
            {
                var split = secretMessage.Split(DELIMITER);
                int iterations = int.Parse(split[ITERATION_INDEX]);
                byte[] salt = Convert.FromBase64String(split[SALT_INDEX]);

                var passwordBytes = new PasswordDeriveBytes(password, salt, HASH_NAME, iterations);
                byte[] keyBytes = passwordBytes.GetBytes(KEY_SIZE / 8);

                symmetricAlgorithm.Mode = CipherMode.CBC;

                using (var plaintext = new MemoryStream())
                {
                    using (ICryptoTransform decryptor = symmetricAlgorithm.CreateDecryptor(keyBytes, iv))
                    {
                        using (var cryptoStream = new CryptoStream(plaintext, decryptor, CryptoStreamMode.Write))
                        {
                            byte[] encryptedMessage = Convert.FromBase64String(split[PBKDF2_INDEX]);
                            cryptoStream.Write(encryptedMessage, 0, encryptedMessage.Length);
                        }
                    }

                    decryptedMessage = Encoding.UTF8.GetString(plaintext.ToArray());
                }
            }

            return decryptedMessage;
        }

        /// <summary>
        /// Decrypts a file's contents.
        /// </summary>
        /// <param name="filePath">The path to the file.</param>
        /// <param name="password">The password to be used for decrypting.</param>
        /// <param name="iv">The initialization vector to use.</param>
        /// <returns>A decrypted message.</returns>
        public void DecryptFile(string filePath, string password, byte[] iv)
        {
            DecryptFile<AesManaged>(filePath, password, iv);
        }
        /// <summary>
        /// Decrypts a file's contents.
        /// </summary>
        /// <typeparam name="T">SymmetricAlgorithm</typeparam>
        /// <param name="filePath">The path to the file.</param>
        /// <param name="password">The password to be used for decrypting.</param>
        /// <param name="iv">The initialization vector to use.</param>
        /// <returns>A decrypted message.</returns>
        public void DecryptFile<T>(string filePath, string password, byte[] iv) where T : SymmetricAlgorithm, new()
        {
            if (!File.Exists(filePath))
                throw new FileNotFoundException();

            string fileContent = File.ReadAllText(filePath);
            string decryptedContent = Decrypt<T>(fileContent, password, iv);

            using (var streamWriter = new StreamWriter(filePath, append: false))
            {
                streamWriter.Write(decryptedContent);
            }
        }

        /// <summary>
        /// Encrypts a message as an asynchronous operation.
        /// </summary>
        /// <param name="secretMessage">The message to encrypt.</param>
        /// <param name="password">The password to be used for encrypting.</param>
        /// <returns>An encrypted byte array.</returns>
        public async Task<string> EncryptAsync(string secretMessage, string password)
        {
            return await EncryptAsync<AesManaged>(secretMessage, password);
        }
        /// <summary>
        /// Encrypts a message as an asynchronous operation.
        /// </summary>
        /// <typeparam name="T">SymmetricAlgorithm</typeparam>
        /// <param name="secretMessage">The message to encrypt.</param>
        /// <param name="password">The password to be used for encrypting.</param>
        /// <returns>An encrypted byte array.</returns>
        public async Task<string> EncryptAsync<T>(string secretMessage, string password) where T : SymmetricAlgorithm, new()
        {
            byte[] encryptedMessage;
            byte[] salt = new byte[SALT_BYTE_SIZE];

            using (var symmetricAlgorithm = new T())
            {
                using (var cryptoProvider = new RNGCryptoServiceProvider())
                {
                    cryptoProvider.GetBytes(salt);
                }

                var passwordBytes = new PasswordDeriveBytes(password, salt, HASH_NAME, PBKDF2_ITERATIONS);
                byte[] keyBytes = passwordBytes.GetBytes(KEY_SIZE / 8);

                symmetricAlgorithm.Mode = CipherMode.CBC;
                this.iv = symmetricAlgorithm.IV;

                using (var ciphertext = new MemoryStream())
                {
                    using (ICryptoTransform encryptor = symmetricAlgorithm.CreateEncryptor(keyBytes, this.iv))
                    {
                        using (var cryptoStream = new CryptoStream(ciphertext, encryptor, CryptoStreamMode.Write))
                        {
                            byte[] ciphertextMessage = Encoding.UTF8.GetBytes(secretMessage);
                            await cryptoStream.WriteAsync(ciphertextMessage, 0, ciphertextMessage.Length);
                        }
                    }

                    encryptedMessage = ciphertext.ToArray();
                }
            }

            return string.Concat(PBKDF2_ITERATIONS, DELIMITER, Convert.ToBase64String(salt), DELIMITER, Convert.ToBase64String(encryptedMessage));
        }

        /// <summary>
        /// Encrypts a file's contents as an asynchronous operation.
        /// </summary>
        /// <param name="filePath">The path to the file.</param>
        /// <param name="password">The password to be used for encrypting.</param>
        /// <returns>An encrypted byte array.</returns>
        public async Task EncryptFileAsync(string filePath, string password)
        {
            await EncryptFileAsync<AesManaged>(filePath, password);
        }
        /// <summary>
        /// Encrypts a file's contents as an asynchronous operation.
        /// </summary>
        /// <typeparam name="T">SymmetricAlgorithm</typeparam>
        /// <param name="filePath">The path to the file.</param>
        /// <param name="password">The password to be used for encrypting.</param>
        /// <returns>An encrypted byte array.</returns>
        public async Task EncryptFileAsync<T>(string filePath, string password) where T : SymmetricAlgorithm, new()
        {
            if (!File.Exists(filePath))
                throw new FileNotFoundException();

            string fileContent = File.ReadAllText(filePath);
            string encryptedContent = await EncryptAsync<T>(fileContent, password);

            using (var streamWriter = new StreamWriter(filePath, append: false))
            {
                await streamWriter.WriteAsync(encryptedContent);
            }
        }

        /// <summary>
        /// Decrypts an encrypted message as an asynchronous operation.
        /// </summary>
        /// <param name="secretMessage">The message to decrypt.</param>
        /// <param name="password">The password to be used for decrypting.</param>
        /// <param name="iv">The initialization vector to use.</param>
        /// <returns>A decrypted message.</returns>
        public async Task<string> DecryptAsync(string secretMessage, string password, byte[] iv)
        {
            return await DecryptAsync<AesManaged>(secretMessage, password, iv);
        }
        /// <summary>
        /// Decrypts an encrypted message as an asynchronous operation.
        /// </summary>
        /// <typeparam name="T">SymmetricAlgorithm</typeparam>
        /// <param name="secretMessage">The message to decrypt.</param>
        /// <param name="password">The password to be used for decrypting.</param>
        /// <param name="iv">The initialization vector to use.</param>
        /// <returns>A decrypted message.</returns>
        public async Task<string> DecryptAsync<T>(string secretMessage, string password, byte[] iv) where T : SymmetricAlgorithm, new()
        {
            string decryptedMessage;

            using (var symmetricAlgorithm = new T())
            {
                var split = secretMessage.Split(DELIMITER);
                int iterations = int.Parse(split[ITERATION_INDEX]);
                byte[] salt = Convert.FromBase64String(split[SALT_INDEX]);

                var passwordBytes = new PasswordDeriveBytes(password, salt, HASH_NAME, iterations);
                byte[] keyBytes = passwordBytes.GetBytes(KEY_SIZE / 8);

                symmetricAlgorithm.Mode = CipherMode.CBC;

                using (var plaintext = new MemoryStream())
                {
                    using (ICryptoTransform decryptor = symmetricAlgorithm.CreateDecryptor(keyBytes, iv))
                    {
                        using (var cryptoStream = new CryptoStream(plaintext, decryptor, CryptoStreamMode.Write))
                        {
                            byte[] encryptedMessage = Convert.FromBase64String(split[PBKDF2_INDEX]);
                            await cryptoStream.WriteAsync(encryptedMessage, 0, encryptedMessage.Length);
                        }
                    }

                    decryptedMessage = Encoding.UTF8.GetString(plaintext.ToArray());
                }
            }

            return decryptedMessage;
        }

        /// <summary>
        /// Decrypts a file's contents as an asynchronous operation.
        /// </summary>
        /// <param name="filePath">The path to the file.</param>
        /// <param name="password">The password to be used for decrypting.</param>
        /// <param name="iv">The initialization vector to use.</param>
        /// <returns>A decrypted message.</returns>
        public async Task DecryptFileAsync(string filePath, string password, byte[] iv)
        {
            await DecryptFileAsync<AesManaged>(filePath, password, iv);
        }
        /// <summary>
        /// Decrypts a file's contents as an asynchronous operation.
        /// </summary>
        /// <typeparam name="T">SymmetricAlgorithm</typeparam>
        /// <param name="filePath">The path to the file.</param>
        /// <param name="password">The password to be used for decrypting.</param>
        /// <param name="iv">The initialization vector to use.</param>
        /// <returns>A decrypted message.</returns>
        public async Task DecryptFileAsync<T>(string filePath, string password, byte[] iv) where T : SymmetricAlgorithm, new()
        {
            if (!File.Exists(filePath))
                throw new FileNotFoundException();

            string fileContent = File.ReadAllText(filePath);
            string decryptedContent = await DecryptAsync<T>(fileContent, password, iv);

            using (var streamWriter = new StreamWriter(filePath, append: false))
            {
                await streamWriter.WriteAsync(decryptedContent);
            }
        }
        #endregion

        #region Private Static Methods
        private static bool SlowEquals(byte[] a, byte[] b)
        {
            var diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
            {
                diff |= (uint)(a[i] ^ b[i]);
            }
            return diff == 0;
        }

        private static byte[] GetPbkdf2Bytes(string password, byte[] salt, int iterations, int outputBytes)
        {
            byte[] result;
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt))
            {
                pbkdf2.IterationCount = iterations;
                result = pbkdf2.GetBytes(outputBytes);
            }

            return result;
        }
        #endregion
    }
}