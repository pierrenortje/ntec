var ntec = {
    crypto: {}
};

ntec.crypto.sym.aesGcm = {

    ///
    /// A function to encrypt AES-GCM
    ///
    encrypt: async function (plaintext, base64Key) {
        const key = await this.importKey(base64Key);
        const encoder = new TextEncoder();

        const nonce = crypto.getRandomValues(new Uint8Array(12)); // 96-bit nonce
        const plaintextBytes = encoder.encode(plaintext);

        const ciphertext = await crypto.subtle.encrypt(
            {
                name: "AES-GCM",
                iv: nonce,
            },
            key,
            plaintextBytes
        );

        return {
            ciphertext: btoa(String.fromCharCode(...new Uint8Array(ciphertext))), // Base64 encode ciphertext
            nonce: btoa(String.fromCharCode(...nonce)), // Base64 encode nonce
        };
    },

    ///
    /// A function to decrypt AES-GCM
    ///
    decrypt: async function (base64Ciphertext, base64Nonce, base64Key, base64Tag) {
        const key = await this.importKey(base64Key);
        const decoder = new TextDecoder();

        let ciphertext = Uint8Array.from(atob(base64Ciphertext), c => c.charCodeAt(0));
        if (base64Tag) {
            const tag = Uint8Array.from(atob(base64Tag), c => c.charCodeAt(0));
            ciphertext = new Uint8Array([...ciphertext, ...tag]);
        }

        const nonce = Uint8Array.from(atob(base64Nonce), c => c.charCodeAt(0));

        const plaintextBytes = await crypto.subtle.decrypt(
            {
                name: "AES-GCM",
                iv: nonce,
            },
            key,
            ciphertext
        );

        return decoder.decode(plaintextBytes);
    },

    importKey: function (base64Key) {
        const rawKey = Uint8Array.from(atob(base64Key), c => c.charCodeAt(0));
        return crypto.subtle.importKey(
            "raw",
            rawKey,
            {
                name: "AES-GCM",
            },
            true,
            ["encrypt", "decrypt"]
        );
    }
}

ntec.crypto.asym.rsa = {
    generateKeyPair: async function () {
        const keyPair = await window.crypto.subtle.generateKey(
            {
                name: "RSA-OAEP",
                modulusLength: 2048, // Key size
                publicExponent: new Uint8Array([1, 0, 1]), // 65537
                hash: { name: "SHA-256" }, // Hash function
            },
            true, // Extractable (allows exporting the keys)
            ["encrypt", "decrypt"] // Key usage
        );
        return keyPair;
    },

    // Encrypt data with the public key
    encryptData: async function (publicKey, data) {
        const encodedData = new TextEncoder().encode(data); // Convert to Uint8Array
        const encryptedData = await window.crypto.subtle.encrypt(
            {
                name: "RSA-OAEP",
            },
            publicKey,
            encodedData
        );
        return encryptedData;
    },

    // Decrypt data with the private key
    decryptData: async function (privateKey, encryptedData) {
        const decryptedData = await window.crypto.subtle.decrypt(
            {
                name: "RSA-OAEP",
            },
            privateKey,
            encryptedData
        );
        return new TextDecoder().decode(decryptedData); // Convert back to string
    },

    // Export a public or private key to JWK format (JSON Web Key)
    exportKey: async function (key) {
        const exportedKey = await window.crypto.subtle.exportKey("jwk", key);
        return exportedKey;
    }
}